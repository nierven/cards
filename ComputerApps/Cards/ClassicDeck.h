/*
 * PaquetCartes.h
 *
 *  Created on: 7 oct. 2023
 *      Author: lorenzo
 */

#ifndef CLASSICDECK_H_
#define CLASSICDECK_H_
#include "Pile.h"

class ClassicDeck : public Pile{
	/**
	 * @brief This class allows generating a standard deck of 52 cards
	 */
public:
	ClassicDeck();
	bool operator<(Card& carte2);
	virtual ~ClassicDeck();
};

#endif /* PAQUETCARTES_H_ */
