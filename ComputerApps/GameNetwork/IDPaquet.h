#pragma once
#include "Paquet.h"

class IDPaquet : public Paquet 
{
private:
	int _idGame;
	int _idPlayer;

public:

	IDPaquet();
	IDPaquet(int idGame, int idPlayer, int sizeMsg);
	IDPaquet(const IDPaquet& paquet);
	IDPaquet(const IDPaquet* paquet);

	int getIDGame() const;
	int getIDPlayer() const;
	
	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);

};

