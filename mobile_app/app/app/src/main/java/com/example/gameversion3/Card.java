package com.example.gameversion3;

import androidx.annotation.NonNull;

import java.util.Random;

/**
 * Classe Card utlisée par tout le monde. Implémente Comparable pour pouvoir trier les cartes
 * plus facilement avec la méthode Collections.sort(liste_de_cartes)
 * Ajout de l'attribut selected permettant de savoir si une carte affichée à l'écran est sélectionnée
 * ou pas
 */
class Card implements Comparable<Card>{

    private Couleur couleur; // Interface
    private Valeur valeur; // Interface
    private boolean selected;
    private int drawableId;

    public Card(Card.Couleur couleur, Valeur valeur) { // Interface
        setCouleur(couleur);
        setValeur(valeur);
        setState(false);
        setDrawableId(0);
    }

    public Card(String stringFromServer) {
        String[] stringsCardSplited = stringFromServer.split(";", 2);

        setCouleur(Couleur.getColorFromInt(Integer.parseInt(stringsCardSplited[0])));
        setValeur(Valeur.getValueFromInt(Integer.parseInt(stringsCardSplited[1])));
        setState(false);
        setDrawableId(0);
    }

    public void setState(boolean b) {
        this.selected = b;
    }

    public boolean getState() {
        return this.selected;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public Valeur getValeur() {
        return valeur;
    }

    public void setValeur(Valeur valeur) {
        this.valeur = valeur;
    }


    @NonNull
    @Override
    public String toString() {
        return this.getStringValeurApp() + "_of_" + this.getStringCouleurApp();
    }

    public String getStringValeurApp() {
        switch( this.valeur ) {
            case zero: return "zero";
            case ace: return "ace";
            case two: return "two";
            case three: return "three";
            case four: return "four";
            case five: return "five";
            case six: return "six";
            case seven: return "seven";
            case eight: return "eight";
            case nine: return "nine";
            case ten: return "ten";
            case jack: return "jack";
            case queen: return "queen";
            case king: return "king";
            default: throw new RuntimeException( "Mauvaise valeur" );
        }
    }


    public String getStringCouleurApp() {
        switch( this.couleur ) {
            case Heart: return "hearts";
            case Spade: return "spades";
            case Clubs: return "clubs";
            case Diamond: return "diamonds";
            case NonDefini: return "undefined";
            default: throw new RuntimeException( "Mauvaise couleur" );
        }
    }

    public String idCard() {
        return this.toStringCouleur() + ";" + this.toStringValeur();
    } // Interface

    private String toStringCouleur() {

        String integer = "0";
        // Interface : convertit enum Couleur en string
        switch (couleur) {
            case NonDefini:
                integer = "0";
                break;
            case Heart:
                integer = "1";
                break;
            case Spade:
                integer = "2";
                break;
            case Clubs:
                integer = "3";
                break;
            case Diamond:
                integer = "4";
                break;
        }
        return integer;
    }


    public String toStringValeur() {

        String integer = "0";
        // Interface : Convertit enum Valeur en string
        switch (valeur) {
            case zero:
                integer = "0";
                break;
            case ace:
                integer = "1";
                break;
            case two:
                integer = "2";
                break;
            case three:
                integer = "3";
                break;
            case four:
                integer = "4";
                break;
            case five:
                integer = "5";
                break;
            case six:
                integer = "6";
                break;
            case seven:
                integer = "7";
                break;
            case eight:
                integer = "8";
                break;
            case nine:
                integer = "9";
                break;
            case ten:
                integer = "10";
                break;
            case jack:
                integer = "11";
                break;
            case queen:
                integer = "12";
                break;
            case king:
                integer = "13";
                break;
        }
        return integer;
    }

    @Override
    public int compareTo(Card card) {
        return (this.getValeur().ordinal() - card.getValeur().ordinal());
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public enum Couleur { // Interface
        Heart, Spade, Clubs, Diamond, NonDefini;
        private static final Random random = new Random();

        private static final Couleur[] couleurs = values();

        public static Couleur getColorFromInt(int value) {
            Card.Couleur color = null;
            switch (value) {
                case 0:
                    color = Couleur.NonDefini;
                    break;
                case 1:
                    color = Couleur.Heart;
                    break;
                case 2:
                    color = Couleur.Spade;
                    break;
                case 3:
                    color = Couleur.Clubs;
                    break;
                case 4:
                    color = Couleur.Diamond;
                    break;
            }
            return color;
        }

        public static Couleur getRandomCouleur() {
            return couleurs[random.nextInt(couleurs.length)];
        }
    }

    public enum Valeur { // Interface
         three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace, two, zero;

        private static final Random randomValeur = new Random();

        private static final Valeur[] valeurs = values();

        public static Valeur getValueFromInt(int value) {
            Card.Valeur valeur = null;
            switch (value) {
                case 0:
                    valeur = Valeur.zero;
                    break;
                case 1:
                    valeur = Valeur.ace;
                    break;
                case 2:
                    valeur = Valeur.two;
                    break;
                case 3:
                    valeur = Valeur.three;
                    break;
                case 4:
                    valeur = Valeur.four;
                    break;
                case 5:
                    valeur = Valeur.five;
                    break;
                case 6:
                    valeur = Valeur.six;
                    break;
                case 7:
                    valeur = Valeur.seven;
                    break;
                case 8:
                    valeur = Valeur.eight;
                    break;
                case 9:
                    valeur = Valeur.nine;
                    break;
                case 10:
                    valeur = Valeur.ten;
                    break;
                case 11:
                    valeur = Valeur.jack;
                    break;
                case 12:
                    valeur = Valeur.queen;
                    break;
                case 13:
                    valeur = Valeur.king;
                    break;
                default:
                    System.out.print("Cette valeur n'existe pas");
            }
            return valeur;
        }

        public static Valeur getRandomValeur() {
            return valeurs[randomValeur.nextInt(valeurs.length)];
        }
    }

}