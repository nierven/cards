#pragma once
#include <iostream>
#include <string>

using namespace std;

enum type_action { RIEN = 0,
	ID,
	DISTRIB,
	ASK,
	ATTENTE,
	REMOVE,
	SKIP,
	PLAY,
	PIOCHE,
	INFO_IA,
	INFO_ECRAN,
	READY,
	PLAYER_NAME,
	CLIENT_PSEUDO,
	PLAYER_READY,
	PLAYER_TURN,
	END_GAME,
	COUP_ILLEGAL,
	START
};

string splitString(string chaine, int* debut);

class Paquet
{
protected:
	int _msgLength;
	type_action _msgFunction;
	//unsigned char* _msg;
	string _msg;
	int _sizeMsg;

public:

	Paquet();
	Paquet(type_action action, int sizeMsg);
	Paquet(const Paquet& paquet);
	Paquet(const Paquet* paquet);

	type_action getMsgFunction() const;

	//virtual unsigned char* encode() = 0;
	virtual string encode() = 0;
	virtual void decode(string receivedMsg) = 0;

	~Paquet();
};

