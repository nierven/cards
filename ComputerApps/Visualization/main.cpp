#include "Visualization.h"

int main()
{
    Visualization visu;
    while (!visu.IsDone())
    {
        visu.HandleInputs();
        visu.Update();
        visu.Render();
    }

    return 0;
}