#include "PlayerTurnPaquet.h"

PlayerTurnPaquet::PlayerTurnPaquet() : Paquet()
{
	this->_idPlayer = 0;
}

PlayerTurnPaquet::PlayerTurnPaquet(int iDPaquet, string pseudo, int sizeMsg) : Paquet(PLAYER_TURN, sizeMsg)
{
	this->_idPlayer = iDPaquet;
	this->_pseudo = pseudo;
}

PlayerTurnPaquet::PlayerTurnPaquet(const PlayerTurnPaquet& paquet) : Paquet(paquet)
{
	this->_idPlayer = paquet._idPlayer;
	this->_pseudo = paquet._pseudo;
}

int PlayerTurnPaquet::getIDPlayer() const
{
	return this->_idPlayer;
}

string PlayerTurnPaquet::getPseudo() const
{
	return this->_pseudo;
}

string PlayerTurnPaquet::encode()
{
	string message;

	message = to_string(this->_idPlayer) + ":" + _pseudo;
	this->_msgLength = message.length();
	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void PlayerTurnPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 4)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_idPlayer = stoi(info);
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_pseudo = info;
					stateDecode++;
				}
				else {
					i++;
				}
				break;

			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
