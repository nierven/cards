#pragma once

#include <map>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "Activity.h"
#include "Client.h"
#include <string>
#include <vector>

using namespace std;

class Visualization
{
public:
	Visualization();

	sf::RenderWindow* getWindow();
	bool IsDone() const;

	void HandleInputs();
	void Update();
	void Render();

	void SwitchTo(ActivityType activityType);

private:
	bool _isDone = false;

	Client* client;
	std::vector<std::string> *player_connectes_noms;

	sf::RenderWindow _window;
	map<ActivityType, Activity*> _activities;
	ActivityType _currentActivityType;
	
};