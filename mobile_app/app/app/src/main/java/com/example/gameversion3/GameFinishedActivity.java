package com.example.gameversion3;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class GameFinishedActivity extends AppCompatActivity {

    boolean YouWin;
    TextView textWin;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game_finished);

        YouWin = getIntent().getBooleanExtra(GameControllerPresident.BOOLEAN_WIN_CODE, false);

        textWin = findViewById(R.id.activity_game_finished_win_txt);

        MediaPlayer musicWin = MediaPlayer.create(this, R.raw.win_sound);
        MediaPlayer musicLose = MediaPlayer.create(this, R.raw.lose_sound);

        Button button_restart = findViewById(R.id.activity_game_finished_restart_btn);
        button_restart.setEnabled(false);

        GameControllerPresident.StaticController.addGameFinishedActivity(this);

        if (YouWin) {
            musicWin.start();
            GameControllerPresident.nbGameWon += 1;
            textWin.setText(R.string.text_win);
        }
        else {
            musicLose.start();
            textWin.setText(R.string.text_lose);
        }


        musicWin.setOnCompletionListener(mediaPlayer -> {
            musicWin.stop();
            musicWin.release();
        });

        musicLose.setOnCompletionListener(mediaPlayer -> {
            musicLose.stop();
            musicLose.release();
        });

        button_restart.setOnClickListener(view -> {
            Intent gameActivity = new Intent(GameFinishedActivity.this, GameActivity.class);
            startActivity(gameActivity);
        });

    }
}
