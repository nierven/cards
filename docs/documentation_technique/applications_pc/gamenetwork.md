# GameNetwork

Le protocole utilisé pour le réseau est du TCP. L'organisation d'un réseau est typiquement la suivante :

![orga_reseau](../../architecture/network.png)

Pour communiquer, la classe **Paquet** a été implémentée comme classe abstraite qu'il faut utiliser comme classe mère lorsque l'on veut créer son propre type de paquet. Le protocole utilisé à l'intérieur d'un paquet est le suivant (avec deux exemples) :

![protocoles](../../architecture/cards-Protocoles.png)

Cette représentation n'est **pas** celle utilisée exactement. L'encodage est en ASCII, l'unité est un uint de 8 bits. Les champs sont séparés en pratique par un ":" pour Paquet et par "/" pour une classe fille de Paquet.

Il existe deux protocoles d'échanges importants qui sont imagés ci-dessous :

![discovery](../../architecture/cards-discovery.png)

Le premier est un échange UDP pour que le client puisse obtenir l'adresse IPv4 du serveur. Il envoie en UDP sur le port 2001 un message à l'adresse de broadcast du sous-réseau. Si le réseau est bien configuré, le serveur est capable de voir ce message et de renvoyer son adresse IP au client en UDP. Une fois l'adresse obtenue, le client peut alors se connecter en TCP au serveur. Il existe un protocole d'introduction entre client et serveur : on échange l'ID de jeu et le nom du joueur avant d'attendre que le joueur soit prêt. Lorsque tous les joueurs sont prêts, le serveur envoie un message de début de partie aux joueurs.

Liste des enums :

- **type_action** (Paquet.h) : Liste des commandes possibles pour le protocole de communication

Liste des classes :

- **Client** (Client.h) : Client TCP pouvant soit se connecter à un serveur TCP soit être instancié comme client du côté du serveur 
- **Serveur** (Serveur.h) : Serveur TCP capable de listening sur un port et de gérer des clients connectés
- **Paquet** (Paquet.h) : Classe abstraite permettant de gérer un paquet
    - **DistribCardPaquet** (DistribCardPaquet.h) : Distribution d'une ou plusieurs cartes (Serveur -> Android)
    - **EndGamePaquet** (EndGamePaquet.h) : Notification de fin de partie (Serveur -> Visualization/Android)
    - **GenericPaquet** (GenericPaquet.h) : Encapsulation d'un string pour la création de sous-protocole ou de commandes sans arguments (possible entre tout le monde)
    - **IDPaquet** (IDPaquet.h) : Information sur le type de jeu joué (Président, Bataille corse, ...) (Serveur -> Visualization/Android)
    - **PlayCardPaquet** (PlayCardPaquet.h) : Jeu d'une ou plusieurs cartes (Android -> Serveur)
    - **PlayerNamePaquet** (PlayerNamePaquet.h) : Information sur les noms des joueurs pour les afficher (Serveur -> Visualization)
    - **PlayerReadyPaquet** (PlayerReadyPaquet.h) : Joueur prêt (Android -> Serveur)
    - **PlayerTurnPaquet** (PlayerTurnPaquet.h) : Information du joueur ayant la main (Serveur -> Visualization)
    - **PseudoClientPaquet** (PseudoClientPaquet.h) : Pseudo d'un nouveau client (Visualization/Android -> Serveur)
    - **RemoveCardPaquet** (RemoveCardPaquet.h) : Suppresion d'une ou plusieurs cartes (Serveur -> Android)
    - **ScreenInfoPaquet** (ScreenInfoPaquet.h) : Informations sur l'état du jeu qu'une Visualization peut afficher (Serveur -> Visualization)