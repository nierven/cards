#include "Client.h"

#define CONDITIONS (this->clientStatus != Socket::Disconnected) && (this->clientStatus != Socket::Error)

Client::Client()
{
	this->player = Player("");
	this->name = "";
	this->idGame = 0;
	this->idPlayer = 0;
	this->listReceivedPaquet = {};
	this->paquetTosend = {};
	this->socket = NULL;
	this->clientStatus = Socket::NotReady;
	this->adress = IpAddress("0.0.0.0");
	this->port = 0;
}

Client::Client(int idGame, int idPlayer, TcpSocket* socket)
{
	this->name = "";
	this->idGame = idGame;
	this->idPlayer = idPlayer;
	this->listReceivedPaquet = {};
	this->paquetTosend = {};
	this->socket = socket;
	this->clientStatus = Socket::NotReady;
	this->adress = IpAddress("0.0.0.0");
	this->port = 0;
}

Client::Client(const Client& client)
{
	this->name = client.name;
	this->idGame = client.idGame;
	this->idPlayer = client.idPlayer;
	this->listReceivedPaquet = client.listReceivedPaquet;
	this->paquetTosend = client.paquetTosend;
	this->socket = client.socket;
	this->clientStatus = client.clientStatus;
	this->adress = client.adress;
	this->port = client.port;
	//this->listeningThread = thread(&Client::getNbWaitPaquet, this) ;
}

Client::Client(IpAddress adress, int port)
{
	this->name = "";
	this->idGame = 0;
	this->idPlayer = 0;
	this->listReceivedPaquet = {};
	this->paquetTosend = {};
	this->socket = NULL;
	this->clientStatus = Socket::NotReady;
	this->adress = adress;
	this->port = port;
}

Client::Client(IpAddress adress, int port, string name)
{
	this->name = name;
	this->idGame = 0;
	this->idPlayer = 0;
	this->listReceivedPaquet = {};
	this->paquetTosend = {};
	this->socket = NULL;
	this->clientStatus = Socket::NotReady;
	this->adress = adress;
	this->port = port;
}

Player* Client::getPlayer()
{
	 return &this->player; 
}

queue<Paquet*>* Client::getListReceivedPaquet()
{
	return &this->listReceivedPaquet;
}

string Client::getName()
{
	return this->name;
}

void Client::addPaquetToSend(Paquet* paquet)
{
	this->mutex.lock();
	this->paquetTosend.push(paquet);
	this->mutex.unlock();
}


void Client::launchConnection()
{
	this->threadConnection = thread(&Client::connectFromClient, this);
}

void Client::connectFromServer()
{
	int sizeCharToReceive = 100;
	char* data = new char[sizeCharToReceive];
	size_t sizeReceived;

	int stateCom = 0;

	while (CONDITIONS && (stateCom < 4)) {
		switch (stateCom)
		{
		case(0):
		{
			cout << "Envoi des ID" << endl;
			//Envoi de l'ID du Jeu et de l'idClient au Client
			//Paquet paquet(ID, this->idGame, this->idPlayer);
			IDPaquet paquet(this->idGame, this->idPlayer, 0);
			string message = paquet.encode();
			cout << message << endl;
			this->clientStatus = this->socket->send(&message[0], (message.length() + 1));
			if (CONDITIONS) {
				stateCom++;
			}
			break;
		}
		case(1):
		{
			cout << "Attente du nom du client" << endl;
			//Attente du nom du client;
			this->clientStatus = this->socket->receive(data, sizeCharToReceive, sizeReceived);
			if (CONDITIONS) {

				//Paquet paquet;
				PseudoClientPaquet paquet;
				string mes;
				mes.assign(data);
				int i = 0;
				type_action action = static_cast<type_action>(stoi(splitString(mes, &i))) ;
				cout << mes << endl;
				if (action == CLIENT_PSEUDO) {
					mutex.lock();
					paquet.decode(mes);
					this->name = paquet.getPseudo();
					cout << this->name << endl;
					mutex.unlock();
					stateCom++;
				}
			}
			break;
		}
		case(2):
		{	
			cout <<"Attente du joueur" << endl;
			//Attente du "Ready" du joueur;
			this->clientStatus = this->socket->receive(data, sizeCharToReceive, sizeReceived);
			if (CONDITIONS) {
				GenericPaquet paquet;
				string mes;
				mes.assign(data);
				paquet.decode(mes);
				cout << mes << endl;
				if (paquet.getMsgFunction() == READY) {
					stateCom++;
					cout << "Fin" << endl;
				}
			}
			break;
		}
		case(3):
		{
			cout << "Lancement des threads" << endl;
			//Boucle principale pour �couter cce qui vient du client
			stateCom++;
			this->sendThread = thread(&Client::receivePaquet, this);
			this->receiveThread = thread(&Client::sendPaquet, this);

			isReady = true;

			break;
		}
		default:
		{
			break;
		}
		}
	}
}

void Client::connectFromClient()
{
	int sizeCharToReceive = 100;
	char* data = new char[sizeCharToReceive];
	size_t sizeReceived;

	int stateCom = 0;
	
	TcpSocket *sock = new TcpSocket;

	if (sock->connect(this->adress, this->port) != Socket::Done) {
		cout << "Connexion impossible" << endl;
	}
	else
	{
		cout << "Visu:connect�" << endl;
		this->socket = sock;
		while (CONDITIONS && (stateCom < 4))
		{
			switch (stateCom)
			{
			case(0):
			{
				cout << "Visu: Reception ID " << endl;
				//Reception de l'ID du Jeu et de l'idClient 
				this->clientStatus = this->socket->receive(data, sizeCharToReceive, sizeReceived);
				if (CONDITIONS) {
					IDPaquet paquet;
					string mes;
					mes.assign(data);
					int i = 0;
					type_action action = static_cast<type_action>(stoi(splitString(mes, &i)));
					if (action == ID) {
						mutex.lock();
						paquet.decode(mes);
						this->idGame = paquet.getIDGame();
						this->idPlayer = paquet.getIDPlayer();
						mutex.unlock();
						stateCom++;
					}
				}
				break;
			}
			case(1):
			{
				cout << "Visu: Envoi nom client" << endl;
				//Envoie du nom du client;
				PseudoClientPaquet paquet(this->name, this->idPlayer, 0);
				string message = paquet.encode();
				this->clientStatus = this->socket->send(&message[0], (message.length() + 1));
				if (CONDITIONS) {
					stateCom++;
				}
				cout << "Visu : Fin envoie nom client" << endl;
				break;
			}
			case(2):
			{	
				cout << "Visu: Envoie Ready" << endl;
				//Envoie du nom du client;
				GenericPaquet paquet(READY,0);
				string message = paquet.encode();
				this->clientStatus = this->socket->send(&message[0], (message.length() + 1));
				if (CONDITIONS) {
					stateCom++;
				}
				cout << "Visu: Fin Envoi nom client" << endl;
				break;
			}
			case(3):
			{	
				cout << "Visu : lancement des thread receive et send" << endl;
				//Boucle principale pour �couter cce qui vient du client
				stateCom++;
				this->sendThread = thread(&Client::receivePaquet, this);
				this->receiveThread = thread(&Client::sendPaquet, this);
				cout << "Visu : Fin" << endl;
				break;
			}
			default:
			{
				break;
			}
			}
		}
	}
}

void Client::close()
{
	this->socket->disconnect();
}

void Client::receivePaquet()
{
	int sizeCharToReceive = 100;
	char* data = new char[sizeCharToReceive];
	size_t sizeReceived;

	cout << "Receive Okay" << endl;

	while (CONDITIONS) {
		
		//Boucle principale pour �couter cce qui vient du client
		this->clientStatus = this->socket->receive(data, sizeCharToReceive, sizeReceived);
		if (CONDITIONS) {
			//Paquet paquet;
			string mes;
			string msgFunct;

			this->mutex.lock();
			mes.assign(data);
			cout << mes << endl;
			int i = 0;
			msgFunct = splitString(mes, &i);
			type_action action = static_cast<type_action>(stoi(msgFunct));

			switch (action)
			{
			case RIEN:

				break;
			case ID:
			{
				IDPaquet* paquet = new IDPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case DISTRIB:
			{
				DistribCardPaquet* paquet = new DistribCardPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case ASK:
			{
				AskCardPaquet* paquet = new AskCardPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case ATTENTE:
			{
				GenericPaquet* paquet = new GenericPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case REMOVE:
			{
				RemoveCardPaquet* paquet = new RemoveCardPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case SKIP:
			{
				GenericPaquet* paquet = new GenericPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case PLAY:
			{
				PlayCardPaquet* paquet = new PlayCardPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case PIOCHE:
			{
				GenericPaquet* paquet = new GenericPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case INFO_IA:
				break;
			case INFO_ECRAN:
			{
				ScreenInfoPaquet* paquet = new ScreenInfoPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case READY:
			{
				GenericPaquet* paquet = new GenericPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case PLAYER_NAME:
			{
				PlayerNamePaquet* paquet = new PlayerNamePaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case CLIENT_PSEUDO:
			{
				PseudoClientPaquet* paquet = new PseudoClientPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case PLAYER_READY:
			{
				PlayerReadyPaquet* paquet = new PlayerReadyPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case PLAYER_TURN:
			{
				PlayerTurnPaquet* paquet = new PlayerTurnPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case END_GAME:
			{
				EndGamePaquet* paquet = new EndGamePaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			case COUP_ILLEGAL:
			{
				GenericPaquet* paquet = new GenericPaquet();
				paquet->decode(mes);
				this->listReceivedPaquet.push(paquet);
				break;
			}
			default:
				break;
			}
			cout << mes << endl;
			this->mutex.unlock();
		}
	}
}

void Client::sendPaquet()
{
	cout << "Send Okay" << endl;
	while (CONDITIONS)
	{
		while (!paquetTosend.empty())
		{
			Paquet* paquet = this->paquetTosend.front();
			string messageToSend = paquet->encode();

			this->mutex.lock();
			this->clientStatus = this->socket->send(&messageToSend[0], (messageToSend.length()+1));
			this->paquetTosend.pop();
			this->mutex.unlock();
		}

		this_thread::sleep_for(200ms);
	}

}

//void Client::pushId()
//{
//	this->server->addIdClient(this->name, this->id);
//}

Client::~Client()
{
	if (this->socket != NULL) {
		delete this->socket;
	}
}

//inline Client& Client::operator=(Client c)
//{
//	Client tmp(c);
//	swap(*this, tmp);
//	return *this;
//}

Client* findVisu(vector<Client*>& listClient) {
	String name = "Visu";

	int i = 0;
	for (int i = 0; i < listClient.size(); i++) {
		if (listClient[i]->getName() == name) {
			return listClient[i];
		}
	}

	return NULL;
	
}
