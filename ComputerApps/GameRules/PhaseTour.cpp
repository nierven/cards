#include "PhaseTour.h"



void PhaseTour::begin(Table& t) {
	//demande de carte au joueur

	//cout, cin
	int indexCard;

	//cout << "Joueur suivant � vous de jouer" << endl;


	//fetching of the Player

	vector<Player*>* temp = t.getPlayers();
	vector<Client*>* clients = this->listClients;
	Player* currentPlayer = NULL;
	

	for (int i = 0;i < clients->size();i++) {
		currentPlayer = (*clients)[i]->getPlayer();
		std::queue<Paquet*>* dataQueue = ((*clients)[i])->getListReceivedPaquet();
		if (not(dataQueue->empty())) {//port priority : player 1 has an advantage
			Paquet* data = dataQueue->front();
			dataQueue->pop();
			

			//If data received
			
			vector<Card> cardsToPlay;
			type_action typeAction=data->getMsgFunction();

			if(typeAction == PLAY){
				PlayCardPaquet* dataPlay=(PlayCardPaquet*)data;

				for (int i = 0; i < dataPlay->getListCard().size(); i++) {
					cardsToPlay.push_back(dataPlay->getListCard()[i]);
				}
				Pile cards = Pile(cardsToPlay);

				//updating action
				this->action = ACTION{ Play,currentPlayer,currentPlayer,currentPlayer->getHand(),t.getPile(0),cards };
			
			}
			if (typeAction == SKIP) {
				Pile cards = Pile();
				this->action = ACTION{ Pass,currentPlayer,currentPlayer,currentPlayer->getHand(),t.getPile(0),cards };
			}
			if (typeAction == RIEN) {
				Pile cards = Pile();
				this->action = ACTION{ Stop,currentPlayer,currentPlayer,currentPlayer->getHand(),t.getPile(0),cards };

			}
		}
	}

	//empty every other data Queue
	for (int i = 0;i < clients->size();i++) {
		std::queue<Paquet*>* dataQueue = ((*clients)[i])->getListReceivedPaquet();
		while (not(dataQueue->empty())) {//port priority : player 1 has an advantage
			dataQueue->pop();
		}
	}

}
bool PhaseTour::legal(Table& t) {

	if (isActionValid(t)) {
		return true;
	}
	cout << "Action non validee : veuillez reesayer \n" << endl;

	return false;
}

void PhaseTour::exec(Table& t) {
	//write down the paquet to send to the client mobile'app
	vector<Card> listCards;
	for (int i = 0; i < this->action.cards.getNbCard(); i++) {
		listCards.push_back( this->action.cards[i]);
	}

	//           ALERTE NEW
	RemoveCardPaquet* paquet = new RemoveCardPaquet(listCards,t.getIdPlayer(this->action.sourcePlayer), 0);//REMOVE, this->action.cards.getNbCard(), listCards, 0, "");
	//           ALERTE NEW : o� on le delete?



	//sending the paquet to the right client
	vector<Client*>::iterator client;
	client = find_if(this->listClients->begin(), this->listClients->end(), [&](Client* const& c) { return this->action.sourcePlayer == c->getPlayer(); });

	if (this->action.type==Play) {//Received action-type is Play
		(*client)->addPaquetToSend(paquet);//send Remove Msg
	}
	//add action to the history
	for (int i = 0;i < this->action.cards.getNbCard();i++) {
		this->action.sourcePlayer->playCard((this->action.cards)[i], *this->action.sourcePile, *this->action.destPile);
	}
	t.addAction(this->action);

	//update the table

	//if someone cutted the defaussse
	bool isCut = false;
	if ((*t.getPile(0)).getNbCard() != 0) {
		if ((*t.getPile(0))[-1].getValue() == 2) isCut = true;
	}
	//if everybody passed
	int nbPlayers = t.getPlayers()->size();
	int nbAction = t.getHistory()->size();
	int personWhoHavePass = 0;
	if (nbAction != 0) {
		for (int i = 0; i < min(nbAction, nbPlayers);i++) {
			if ((*t.getHistory())[nbAction - i - 1].type == Pass) personWhoHavePass++;
		}
	}
	if (personWhoHavePass == nbPlayers - 1) isCut = true;

	//un carr� a �t� compl�t�
	Pile* defausse = t.getPile(0);
	if (defausse->getNbCard()>4){
		if((*defausse)[-1].getValue()== (*defausse)[-4].getValue()){
			isCut=true;
		}
	}
	
	if (isCut) {
		Player dealer;
		int n = t.getPile(0)->getNbCard();
		for (int i = 0; i < n; i++) {
			dealer.playCard(0, *t.getPile(0), *t.getPile(1));

		}
	}

	updateUI(t);

	//update winner table
	if (this->action.sourcePlayer->getHand()->getNbCard() == 0) {
		int nextRank = 1;
		int lastRank = t.getPlayers()->size();

		//search the nextRank
		vector<int> bufferRanking = *t.getRankings();
		std::sort(bufferRanking.begin(), bufferRanking.end());
		for (int elem : bufferRanking) {
			if (elem = nextRank) nextRank++;
		}

		//search the lastRank
		for (int i = bufferRanking.size() - 1; i>=0; i--) {
			if (bufferRanking[i] = lastRank) lastRank--;
		}

		if (this->action.cards[0].getValue() == 2) {
			(*t.getRankings())[t.getIdPlayer(this->action.sourcePlayer)] = lastRank;
		}
		else {
			(*t.getRankings())[t.getIdPlayer(this->action.sourcePlayer)] = nextRank;
		}
	}
}

void PhaseTour::end(Table& t)
{
	if (not(isWinner(t))) {
		Phase::CurrentPhase = new PhaseTour();//play the next player with PhaseTour
		Phase::CurrentPhase->setListClient(this->listClients);
	}
	else {
		//Victory : should create another CurrentPhase : PhaseFinish
		
		//ordering list by handsize, 0 is the winner
		vector<Client*> handsize;
		//vector<string> names;

		for (int i = 0;i < this->listClients->size();i++) {
			//on selectionne les joeurs
			Client* client = (*this->listClients)[i];
			if (client->getName() != "Visu") {
				handsize.push_back(client);
			}
		}

		sort(handsize.begin(), handsize.end(), [&](Client* const& j, Client* const& k) { return (*t.getRankings())[t.getIdPlayer(j->getPlayer())] < (*t.getRankings())[t.getIdPlayer(k->getPlayer())]; });

		vector <string> win;
		for (auto& c : handsize) {
			win.insert(win.begin(), c->getName());
		}

		EndGamePaquet* paquet = new EndGamePaquet(win, win.size());

		Client* client = *find_if(this->listClients->begin(), this->listClients->end(), [&](Client* const& c) { return c->getName() == "Visu"; });
		client->addPaquetToSend(paquet);

		exit(0);
	}
}


bool PhaseTour::isActionValid(Table& t) {

	

	ActionType actionType = this->action.type;
	//check action

	switch (actionType) {
	case Play: {
		// On v�rifie si une action a �t� effectu�e
		if (action.sourcePlayer == NULL) return false;

		//first action
		if (t.getHistory()->size() == 0) {
			return true;
		}

		//A player must play one of their cards
		Player* player = this->action.sourcePlayer;
		Pile* handPlayer = player->getHand();
		Card cardRef = this->action.cards[0];
		int nbCardsToPlay = this->action.cards.getNbCard();
		for (int i = 0; i < nbCardsToPlay; i++) {
			cardRef = this->action.cards[i];
			if (not(handPlayer->isInsidePile(action.cards[i]))) {
				return false;
			}
		}

		//The cards discarded by the player must be identical in value
		for (int i = 0; i < nbCardsToPlay; i++) {
			if (cardRef.getValue() != this->action.cards[i].getValue()) {
				//cout << "Cards have not the same value" << endl;
				return false;
			}
		}

		//The number of cards placed must be equal to the previous move
		int nbCardPlayed;
		if ((t.getPile(0)->getNbCard() != 0) and (nbCardsToPlay != 0)) {
			int i = t.getHistory()->size() - 1;
			do {
				ACTION a = (*t.getHistory())[i];
				nbCardPlayed = a.cards.getNbCard();
				i--;
			} while (nbCardPlayed == 0);
			if (nbCardPlayed != nbCardsToPlay) return false;
		}

		//The cards played must be higher or equal than the previous ones
		Pile* discard = t.getPile(0);
		if (discard->getNbCard() != 0) {
			Card previousCard = (*discard)[-1];
			if (cardRef < previousCard) return false;
		}

		//Checking player
		Player* previousPlayer = (*t.getHistory())[t.getHistory()->size() - 1].sourcePlayer;
		if (previousPlayer == player) {//previous player is the current player
			//Check player have cut
			if ((discard->getNbCard() == 0) and (this->action.type == Play)) {
				return true;
			}
			else {
				return false;
			}
		}
		else if (t.getIdPlayer(player) == (t.getIdPlayer(previousPlayer) + 1) % t.getPlayers()->size()) {
			return true;
		} else {
			if (discard->getNbCard() > 0) {
				return (*discard)[-1] == this->action.cards[0];
			}
			return false;
		}
		break;
	}
	case Pass: { //A player can pass only when its his turn
		ACTION previous = (*t.getHistory())[t.getHistory()->size() - 1];
		int idPrevious = t.getIdPlayer(previous.sourcePlayer);
		int idActuel = t.getIdPlayer(this->action.sourcePlayer);

		if (idActuel != (idPrevious + 1) % t.getPlayers()->size()) {
			return false;
		}
		//else
		return true;

		break;
	}
	case Give: {
		return false;
		break;
	}
	case Stop: {
		return false;
		break;
	}
	default: {
		return false;
	}

	}

	//Partie courcircuiter
	//check player
	
	//pb si l'action d'avant est un skip
	//on s'en fiche si la derniere action �tait un Pass, un Skip, une erreur.
	//Tout ce qui nous int�resse est l'ETAT DE LA PILE
	ACTION previous = (*t.getHistory())[t.getHistory()->size() - 1];
	Player* source = previous.sourcePlayer;
	Pile* defausse = t.getPile(0);

	int idPrevious = t.getIdPlayer(source);
	int idActuel = t.getIdPlayer(this->action.sourcePlayer);

	//apres une coupe/carr�, le dernier joueur rejoue (distincts de t.history.size==0/debut de la partie)
	if (defausse->getNbCard() == 0) {
		if (idPrevious == idActuel) { return true; }
		else { return false; }
	}
	//starting here, defausse.NB !=0
	
	if (this->action.cards.getNbCard() != 0) {//action should always be !=0
		//cant replay after own turn
		if (idPrevious == idActuel) {
			return false;
		}
		//any other can play, if value are ==
		else if ((*defausse)[-1].getValue() == this->action.cards[0].getValue()) {
			//return true;
			//line 202 is already checking
		}
		else {//only next player can play, if values are !=, 
			if (idActuel != (idPrevious + 1) % t.getPlayers()->size()) {
				return false;
			}
			//if idActuel==idPrevious+1,return true
		}
	}


	return true;
}

bool PhaseTour::isWinner(Table& t)
{	
	//check if the rankings is full
	for (int i = 0; i < t.getPlayers()->size();i++) {
		if ((*t.getRankings())[i] == 0) return false;
	}
	return false;
}

void PhaseTour::updateUI(Table& t) {
	int taillePile = t.getPile(0)->getNbCard();
	vector<Card> listCard;

	for (int i = 0; i < taillePile; i++) {
		listCard.push_back( (*t.getPile(0))[i]);
	}

	int nbPlayers = t.getPlayers()->size();
	vector<int> nbCardByPlayer;
	int i = 0;
	for (auto const& player : *t.getPlayers()) {
		nbCardByPlayer.push_back( player->getHand()->getNbCard());
		i++;
	}
	//                NEW??? ALERTE MEMORY LEAK
	ScreenInfoPaquet* paquet =new ScreenInfoPaquet(listCard,nbCardByPlayer,0);//(INFO_ECRAN, taillePile, listCard, nbPlayers, nbCardByPlayer);
	//NEW 
	Client* client = *find_if(this->listClients->begin(), this->listClients->end(), [&](Client* const& c) { return c->getName() == "Visu"; });
	client->addPaquetToSend(paquet);// Action


	//send name of the next player
	int idActuel = t.getIdPlayer(this->action.sourcePlayer);
	int idNext = (idActuel + 1) % t.getPlayers()->size();

	Player* next = *find_if(t.getPlayers()->begin(), t.getPlayers()->end(), [&](Player* const& p) { return t.getIdPlayer(p) == idNext; });
	PlayerTurnPaquet* turn = new PlayerTurnPaquet(idNext,next->getName(), 0);



	

	client->addPaquetToSend(turn);
	// liste cartes au centre (pointeur vers carte)
	// pointeur vers int , tableau de 4 int (nombre de cartes par joueur)
}
