#include "GenericPaquet.h"

GenericPaquet::GenericPaquet() : Paquet()
{
}

GenericPaquet::GenericPaquet(type_action action, int sizeMsg) : Paquet(action, sizeMsg)
{
}

GenericPaquet::GenericPaquet(const GenericPaquet& paquet) : Paquet(paquet)
{
}

string GenericPaquet::encode()
{
    string message;
    this->_msgLength = message.length();

    this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength);

    return this->_msg;
}

void GenericPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	int nbPayer = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 2)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					//i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;

			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
