#include "RemoveCardPaquet.h"

RemoveCardPaquet::RemoveCardPaquet() : Paquet()
{
	this->_listCard = {};
	this->_origin = 0;
}

RemoveCardPaquet::RemoveCardPaquet(vector<Card> listCard, int origin, int sizeMsg) : Paquet(REMOVE, sizeMsg)
{
	this->_listCard = listCard;
	this->_origin = origin;
}

RemoveCardPaquet::RemoveCardPaquet(const RemoveCardPaquet& paquet) : Paquet(paquet)
{
	this->_listCard = paquet._listCard;
	this->_origin = paquet._origin;
}

vector<Card> RemoveCardPaquet::getListCard() const
{
	return this->_listCard;
}

int RemoveCardPaquet::getOrigin() const
{
	return this->_origin;
}

string RemoveCardPaquet::encode()
{
	string message;
	message = to_string(this->_origin) + ":" + to_string(this->_listCard.size()) + ":";

	for (int i = 0; i < this->_listCard.size(); i++) {
		message += this->_listCard[i].idCard();
		if (i != (this->_listCard.size()-1)) {
			message += "/";
		}
	}

	this->_msgLength = message.length();

	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void RemoveCardPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	int nbCard = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 5)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_origin = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					nbCard = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(4):
				for (int j = 0; j < nbCard; j++) {
					info = splitString(receivedMsg, &i);
					this->_listCard.push_back(Card(info));
					if(j != nbCard-1)
						i++;
				}
				stateDecode++;
				break;
			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
