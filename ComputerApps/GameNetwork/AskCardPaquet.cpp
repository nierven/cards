#include "AskCardPaquet.h"

AskCardPaquet::AskCardPaquet() : Paquet()
{
	this->_numberCards = 0;
}

AskCardPaquet::AskCardPaquet(int numberCards, int sizeMsg) : Paquet(ASK, sizeMsg)
{
	this->_numberCards = numberCards;
}

AskCardPaquet::AskCardPaquet(const AskCardPaquet& paquet) : Paquet(paquet)
{
	this->_numberCards = paquet._numberCards;
}

int AskCardPaquet::getNumberCards() const
{
	return this->_numberCards;
}

string AskCardPaquet::encode()
{
	string message;

	message = to_string(this->_numberCards);
	this->_msgLength = message.length();
	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void AskCardPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 3)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_numberCards = stoi(info);
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			
			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
