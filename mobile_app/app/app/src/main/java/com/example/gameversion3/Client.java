package com.example.gameversion3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class Client {

    private PrintWriter output;
    private BufferedReader input;
    private ReentrantLock mutex = new ReentrantLock();
    private ArrayList<String> listMessage;

    public Client() {
        listMessage = new ArrayList<>();

        new Thread(new ClientThread()).start();

        //new Thread(new FindIPAdressBroadcast()).start();

    }

    public void sendMessage(String message) {
        new Thread(new SendToServer(message)).start();
    }

    class FindIPAdressBroadcast implements Runnable {

        @Override
        public void run() {
            while(true) {
                try
                {
//                    DatagramSocket socket = new DatagramSocket(GameController.PORT_UDP);
//                    socket.setBroadcast(true);
//                    String data="BROADCAST";
////                    InetAddress ipadress = InetAddress.getByName("192.168.119.165");
//
//
//                    DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
//                            GameController.broadcastAddress, GameController.PORT_UDP);
//                    socket.send(packet);
//                    socket.close();

                    // InetAddress inetAddress = InetAddress.getByName("255.255.255.255");
                    // GameController.broadcastAddress
                    DatagramSocket newSocket = new DatagramSocket(GameControllerPresident.PORT_UDP, GameControllerPresident.broadcastAddress);
                    //newSocket.setBroadcast(true);
                    byte[] buf = new byte[128];

                    DatagramPacket packet2 = new DatagramPacket(buf, buf.length);
                    newSocket.receive(packet2);
                    GameControllerPresident.IP_ADRESS = packet2.getAddress().toString().split("/")[1];
                    break;

                }
                catch (Exception e)
                {
                    GameControllerPresident.StaticController.printReceivedMessage("Broascast en cours");
                }
            }
            if (GameControllerPresident.IP_ADRESS != null) {
                new Thread(new ClientThread()).start();
            }
        }
    }

    class ClientThread implements Runnable {
        @Override
        public void run() {

            while(true) {
                try {
                    Socket client = new Socket();
                    client.connect(new InetSocketAddress(GameControllerPresident.IP_ADRESS, GameControllerPresident.PORT_TCP), 400);
                    output = new PrintWriter(client.getOutputStream());
                    input = new BufferedReader(new InputStreamReader(client.getInputStream()));

                    GameControllerPresident.StaticController.printReceivedMessage("Serveur trouvé ! Attente des autres joueurs ...");

                    new Thread(new ReceivingFromServer()).start();
                    new Thread(new AnalyseDataReceived()).start();

                    break;

                } catch (SocketTimeoutException e) {
                    GameControllerPresident.StaticController.printReceivedMessage("Pas de serveur trouvé");
                }
                catch (UnknownHostException e) {
                    GameControllerPresident.StaticController.printReceivedMessage("Pas de connexion internet ou mauvaise adresse IP");
                }
                catch (IOException e){
                    throw new RuntimeException(e);
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    class ReceivingFromServer implements Runnable {

        @Override
        public void run() {
            String message = "";
            int carReceived;
            char car;
            while (true) {
                try {
                    carReceived = input.read();
                    if (carReceived >= 0) {
                        car = (char) carReceived;
                        if (car == '\u0000') {
                            mutex.lock();
                            String messageString = message;
                            listMessage.add(0, messageString);
                            message = "";
                            mutex.unlock();
                        }
                        else {
                            message += car;
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

        }
    }

    class AnalyseDataReceived implements Runnable {

        @Override
        public void run() {
            while (true) {
                if (listMessage.size() > 0) {
                    mutex.lock();
                    String lastMessage = listMessage.get(listMessage.size() - 1);
                    boolean beforeDistrib = GameControllerPresident.StaticController.analyseDataReceived(lastMessage);
                    if (GameControllerPresident.isResumed ^ beforeDistrib) {
                        listMessage.remove(listMessage.size() - 1);
                    }
                    mutex.unlock();
                }
            }
        }
    }

    class SendToServer implements Runnable {
        private final String message;

        public SendToServer(String message) {
            this.message = message;
        }
        @Override
        public void run() {
            output.write(message + '\u0000');
            output.flush();
        }
    }
}
