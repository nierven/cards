#include "PhaseStart.h"
#include <thread>

void PhaseStart::begin(Table& t)
{
	//attendre que tous les joueurs soient l�

	cout << "attente de l'arriv�e d'un joueur dans GameTable";//en attente debut partie
	//Send okay to every player

	for (Client* c : *this->listClients)
	{
		GenericPaquet* start = new GenericPaquet(START, 0);
		c->addPaquetToSend(start);
	}

	this_thread::sleep_for(1000ms);

}
bool PhaseStart::legal(Table& t) {
	if (t.getPlayers()->size() >= 1) { return true; }//bidon pour l'instant
	return false;

}

void PhaseStart::exec(Table& t) {//pas de const, seuls les exec ont le droit de modifier la table

	//Modifying Table

	//	creating the piles
	t.addPile(2);
	ClassicDeck deck;
	Player dealer;
	deck.shuffle();
	int i = 0;
	int n = t.getPlayers()->size();
	while (deck.getNbCard() > 25) {
		dealer.playCard(deck[0], &deck, (*t.getPlayers())[i]->getHand());
		i = (i+1) % n;
	}
	
	//send dealing cards
	vector<Card> cardToSend;
	DistribCardPaquet* dealingPaquet;
	for (Client* c : *this->listClients) {
		if (c->getName() != "Visu") {
			Player* currentPlayer = c->getPlayer();
			Pile currentHand = *currentPlayer->getHand();
			int n = currentHand.getNbCard();
			cardToSend.clear();
			for (int i = 0; i < n; i++) {
				cardToSend.push_back(currentHand[i]);
			}
			dealingPaquet = new DistribCardPaquet(cardToSend, 0);
			c->addPaquetToSend(dealingPaquet);
		}
	}

	//init rankings
	for (int& r : *t.getRankings()) {
		r = 0;
	}

	//Rachida screen startup
	//Pile vide
	vector<Card>listCard={};
	


	//number of cards
	int nbPlayers = t.getPlayers()->size();
	vector<int> nbCardByPlayer;
	i = 0;
	for (auto const& player : *t.getPlayers()) {
		nbCardByPlayer.push_back(player->getHand()->getNbCard());
		i++;
	}

	ScreenInfoPaquet* paquet = new ScreenInfoPaquet(listCard, nbCardByPlayer, 0);//(INFO_ECRAN, taillePile, listCard, nbPlayers, nbCardByPlayer);
	//NEW 
	Client* client = *find_if(this->listClients->begin(), this->listClients->end(), [&](Client* const& c) { return c->getName() == "Visu"; });
	client->addPaquetToSend(paquet);// Action

	
}

//will be executed immediatly
void PhaseStart::end(Table& t) {
	//start the player turns

	Phase::CurrentPhase = new PhaseTour();//phase tour qui elle meme appelle Phase joueur
	Phase::CurrentPhase->setListClient(this->listClients);

}



