package com.example.gameversion3;


import java.util.ArrayList;
import java.util.Objects;

public class PlayCardPaquet extends Paquet{

    private ArrayList<Card> _listCard;
    private int _origin;
    private int _destination;

    public PlayCardPaquet(){
        super();
        _listCard = new ArrayList<>();
        _origin = 0;
        _destination = 0;
    }

    public PlayCardPaquet(ArrayList<Card> listCard, int origin, int destination){
        super(type_action.PLAY);
        _listCard = listCard;
        _origin = origin;
        _destination = destination;
    }

    public PlayCardPaquet(PlayCardPaquet paquet){
        super(paquet);
        _listCard = paquet._listCard;
        _origin = paquet._origin;
        _destination = paquet._destination;
    }

    public ArrayList<Card> get_listCard(){
        return _listCard;
    }

    public int get_origin(){
        return _origin;
    }

    public int get_destination(){
        return _destination;
    }

    @Override
    public String encode() {
        String message;
        message = _origin + ":" + _destination + ":" + _listCard.size() + ":";

        for(int i = 0; i<_listCard.size(); i++){
            message += _listCard.get(i).idCard();
            if( i != _listCard.size() - 1){
                message += "/";
            }
        }
        _msgLength = message.length();
        _msg = _msgFunction.toString() + ":" + _msgLength + ":" + message;
        return _msg;
    }

    @Override
    public void decode(String receivedMsg) {

        String[] infoSplited;
        int state = 0;
        int nbCard=0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 5) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        _origin = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (!Objects.equals(infoSplited[3], "")) {
                        _destination = Integer.parseInt(infoSplited[3]);
                    }
                    break;
                case 4:
                    state ++;
                    if (!Objects.equals(infoSplited[4], "")) {
                        nbCard = Integer.parseInt(infoSplited[4]);
                    }
                    break;
                case 5:
                    state ++;
                    if (!Objects.equals(infoSplited[5], "")) {
                        String[] card = infoSplited[5].split("/");
                        for(int j = 0; j<nbCard; j++){
                            _listCard.add(new Card(card[j]));
                        }
                    }
                    break;
            }
        }
    }
}
