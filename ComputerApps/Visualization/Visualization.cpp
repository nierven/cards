#include "Visualization.h"
#include "Activity_Setup.h"
#include "Activity_Game.h"
#include "Activity_Results.h"
Visualization::Visualization()
{
	//client = new Client(IpAddress("192.168.1.62"), 2000, "Visu");
	//
	client = new Client(IpAddress("192.168.43.72"), 2000, "Visu");//pour GA
	// Initialize the SFML window
	_window.create(sf::VideoMode(800, 600), "Cards");

	// Initialize all activities
	_activities[ActivityType::Setup] = new Activity_Setup(this,client);
	_activities[ActivityType::Game] = new Activity_Game(this, client);
	_activities[ActivityType::Results] = new Activity_Results(this, client);

	// Switch to the first activity
	SwitchTo(ActivityType::Setup);
}

bool Visualization::IsDone() const
{
	return _isDone;
}

sf::RenderWindow* Visualization::getWindow()
{
	return &_window;
}

void Visualization::HandleInputs()
{
	sf::Event event;
	while (_window.pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
				client->close();
				// Window got closed externally
				_isDone = true;
				break;
		}
	}
}

void Visualization::Update()
{
	// Update the running activity
	_activities[_currentActivityType]->Update();
}

void Visualization::Render()
{
	// Clear then render and display the current activity
	_window.clear(sf::Color::Black);
	_activities[_currentActivityType]->Render();
	_window.display();
}

void Visualization::SwitchTo(ActivityType activityType)
{
	// Deactivate the current activity
	_activities[_currentActivityType]->Deactivate();

	_currentActivityType = activityType;

	// If game, pass along the list of players
	if (_currentActivityType == ActivityType::Game)
	{
		Activity_Setup* setup = (Activity_Setup*)_activities[ActivityType::Setup];
		Activity_Game* game = (Activity_Game*)_activities[ActivityType::Game];

		game->setListNames(setup->getListNames());
	}

	// Init and switch to the new one
	_activities[_currentActivityType]->Init();
	_activities[_currentActivityType]->Activate();
}