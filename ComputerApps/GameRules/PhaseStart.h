#ifndef PHASESTART_H_
#define PHASESTART_H_

#include "Phase.h"
#include "PhaseTour.h"
#include "Paquet.h"

class PhaseStart : public Phase
{
public:
	void begin(Table& t);
	bool legal(Table& t);
	void exec(Table& t);
	void end(Table& t);
};

#endif;