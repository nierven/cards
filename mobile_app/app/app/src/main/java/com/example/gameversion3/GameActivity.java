package com.example.gameversion3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;



import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Equivalent de View dans le modèle MVP
 * Activité Game correspondant à l'écran principal du jeu affichant les bouttons et les cartes
 * nécessaires au déroulement d'une partie
 * Taille des cartes :
 * hauteur : moitié de la hauteur de l'écran
 * largeur : respecte les proportions de l'image png de la carte
 */

public class GameActivity extends AppCompatActivity  {
    private Button button_play_cards;
    private Button button_play_music;
    private TextView information_txt;
    static final int marge = 30;
    static int screenHeight;
    static int screenWidth;
    static int cardWidth;
    static int cardHeight;
    private final double ratioPictureCard = 500.0/726;
    private MediaPlayer musicDistribCards;
    private MediaPlayer musicMario;
    private MediaPlayer musicDrawCard;
    private MediaPlayer musicPlayCards;
    private MediaPlayer musicShuffleCards;
    static final int nbCardsLine = 13;
    private CustomView customView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Utilisation du WindowManager pour récupérer les dimensions de l'écran
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Hauteur de l'écran stockée dans screenHeight
        screenWidth = size.x;
        screenHeight = size.y;
        double cardInterWidth = ratioPictureCard * screenHeight / 2;
        cardWidth = (int) (cardInterWidth);
        cardHeight = screenHeight/2;

        Log.i("Problème", "avant contentview");
        setContentView(R.layout.activity_game);
        Log.i("Problème", "après contentview");

        // Fait le lien entre le fichier xml activity_game.xml et le code en java
        Button button_draw = findViewById(R.id.activity_game_draw_cards_btn);
        button_play_music = findViewById(R.id.activity_game_play_music_btn);
        button_play_cards = findViewById(R.id.activity_game_play_cards_btn);
        button_play_cards.setEnabled(false);
        Button button_pass_turn = findViewById(R.id.activity_game_pass_turn);
        information_txt = findViewById(R.id.activity_game_info_txt);

        customView = findViewById(R.id.custom_view);

        // Récupération du pseudo du joueur
        String pseudo = getIntent().getStringExtra(MainActivity.PSEUDO_CODE);
        information_txt.append(pseudo);

        // Création du son de distribution des cartes et de la musique de fond
        musicDistribCards = MediaPlayer.create(this, R.raw.distribute_cards);
        musicShuffleCards = MediaPlayer.create(this, R.raw.shuffle_cards);
        musicShuffleCards.start();
        musicMario = MediaPlayer.create(this, R.raw.mario_music);
        musicDrawCard = MediaPlayer.create(this, R.raw.coin_draw_card);
        musicPlayCards = MediaPlayer.create(this, R.raw.minecraft_death_sound);



        // création du controlleur et du GameModel comme argument du constructeur de Presenter
        GameControllerPresident.StaticController.addGameActivity(this, new GameModel());

//        GameController.StaticController.distributeHand();

        this.customView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                boolean treated;

                treated = GameControllerPresident.StaticController.onHandClick(motionEvent);

                return treated;
            }
        });

        // Création de 3 OnClickListener sur chacun des 3 boutons qui nous intéressent
        // Permet de réaliser une action lors de l'appui sur un bouton
        button_play_music.setOnClickListener(view -> {
            if (musicMario.isPlaying()) {
                musicMario.pause();
                button_play_music.setText(R.string.button_play_music);
            } else {
                musicMario.start();
                button_play_music.setText(R.string.button_pause_music);
            }
        });

        button_draw.setOnClickListener(view -> {
            gameFinished(false);
            //GameControllerPresident.StaticController.onButtonDrawClick();
        });

        button_play_cards.setOnClickListener(view -> GameControllerPresident.StaticController.onButtonPlayClick());

        button_pass_turn.setOnClickListener(view -> {
            GenericPaquet paquet = new GenericPaquet(Paquet.type_action.SKIP);
            GameControllerPresident.StaticController.sendMessage(paquet.encode());
        });

        musicShuffleCards.setOnCompletionListener(mediaPlayer -> {
            musicShuffleCards.stop();
            musicShuffleCards.reset();
            musicDistribCards.start();
        });

        musicMario.setOnCompletionListener(mediaPlayer -> {
            musicMario.stop();
            musicMario.reset();
            musicMario.start();
        });

    }

    // Méthode onTouchEvent permettant de gérer ce qu'il se passe lorsqu'on touche la moitié basse
    // de l'écran (zone où se situent les cartes)


    // Permet d'afficher à l'écran l'ensemble des cartes contenues dans l'ArrayList hand
    // Pour cela, on appelle la méthode update de la classe CustomView
    public void distributeHand(ArrayList<Card> hand) {

        int nbCards = hand.size();
        Drawable[] layers = new Drawable[nbCards];
        ArrayList<AnimatedDrawableCard> listAnimatedDrawables = new ArrayList<>();

        for (int i=0; i<nbCards; i++) {

            int drawableId = 0;
            GameControllerPresident.listNumberTranslationCards.add(0);

            if (hand.get(i).getValeur() == Card.Valeur.zero) {
                drawableId = R.drawable.joker;
            }
            else if (hand.get(i).getCouleur() == Card.Couleur.NonDefini) {
                drawableId = R.drawable.nondefini;
            }
            else {
                try {
                    Class<R.drawable> res = R.drawable.class;
                    Field field = res.getField(hand.get(i).toString());
                    drawableId = field.getInt(null);
                }
                catch (Exception e) {
                    Log.e("MyTag", "Failure to get drawable id.", e);
                }
            }

            Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), drawableId);
            hand.get(i).setDrawableId(drawableId);

            int reste = i%nbCardsLine;

            int left = (reste*screenWidth)/15 + marge;
            int top = -GameActivity.screenHeight/2;
            int right = cardWidth + left;
            int bottom = top + cardHeight;

            assert drawable != null;
            drawable.setBounds(left, top, right, bottom);

            AnimatedDrawableCard animatedDrawableCard = new AnimatedDrawableCard(drawable, i);

            listAnimatedDrawables.add(animatedDrawableCard);

            layers[i] = animatedDrawableCard;

        }
        LayerDrawable layerdrawable = new LayerDrawable(layers);
        this.customView.update(layerdrawable, listAnimatedDrawables);
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (musicMario != null) {
            musicMario.release();
        }
        if (musicDrawCard != null) {
            musicDrawCard.release();
        }
        if (musicDistribCards != null) {
            musicDistribCards.release();
        }
        if (musicPlayCards != null) {
            musicPlayCards.release();
        }
        if (musicShuffleCards != null) {
            musicShuffleCards.release();
        }
    }

    public void playSoundDistributeCard() {
        musicDistribCards.start();
    }

    public void playSoundPlayCard() {
        musicPlayCards.start();
    }

    public void updateCustomView() {
        this.customView.invalidate();
    }

    public LayerDrawable getLayerDrawable() {
        return this.customView.getLayerDrawable();
    }

    public ArrayList<AnimatedDrawableCard> getAnimatedDrawableCardList() {
        return this.customView.getAnimatedDrawableCardList();
    }

    public void printReceivedMessage(String message) {
        information_txt.setText(message);
    }

    public void enablePlayButton() {
        button_play_cards.setEnabled(true);
    }

    public void removeCardFromCustomView(int j) {
        ArrayList<AnimatedDrawableCard> animatedDrawableCardList = getAnimatedDrawableCardList();
        animatedDrawableCardList.remove(j);
        GameControllerPresident.listNumberTranslationCards.remove(j);
        for (int i=j; i<animatedDrawableCardList.size(); i++) {
            GameControllerPresident.listNumberTranslationCards.set(i, GameControllerPresident.listNumberTranslationCards.get(i) + 1);
        }

        Drawable[] newLayers = new Drawable[GameControllerPresident.nbCards];

        for (int i = 0; i< GameControllerPresident.nbCards; i++) {
                newLayers[i] = animatedDrawableCardList.get(i);
        }

        LayerDrawable newLayerDrawable  = new LayerDrawable(newLayers);
        customView.update(newLayerDrawable, animatedDrawableCardList);
    }

    public void translateCards() {
        ArrayList<AnimatedDrawableCard> animatedDrawableCardList = getAnimatedDrawableCardList();
        for (int i=0; i < animatedDrawableCardList.size(); i++) {
            animatedDrawableCardList.get(i).animateOnCardPlayed(GameControllerPresident.listNumberTranslationCards.get(i));
            GameControllerPresident.listNumberTranslationCards.set(i, 0);
        }
        customView.setAnimatedDrawableCardList(animatedDrawableCardList);
        customView.invalidate();

    }

    public void gameFinished(boolean YouWin) {
        Intent gamefinishedActivity = new Intent(GameActivity.this, GameFinishedActivity.class);
        gamefinishedActivity.putExtra(GameControllerPresident.BOOLEAN_WIN_CODE, YouWin);
        Log.i("Debug", "Démarrage de StartingGameActivity");
        startActivity(gamefinishedActivity);
    }


    @Override
    protected void onResume() {
        super.onResume();

        GameControllerPresident.isResumed = true;

    }

    @Override
    protected void onPause() {
        super.onPause();

        GameControllerPresident.isResumed = false;
    }


}