/*
 * Table.h
 *
 *  Created on: 8 oct. 2023
 *      Author: lorenzo
 */

#ifndef TABLE_H_
#define TABLE_H_
#include "ClassicDeck.h"
#include "Pile.h"
#include "Player.h"

enum ActionType{
	Play = 0,
	Give,
	Pass,
	Stop
};

struct ACTION{
	ActionType type = Play;
	Player *sourcePlayer = NULL;
	Player *destPlayer = NULL;
	Pile *sourcePile = NULL;
	Pile *destPile = NULL;
	Pile cards; //J'ai pris quelques libertés dessus par rapport au graph
};


class Table {
	/**
	*The table contains only players who are sit down, cards on the game board
	*but any RULES, do code rules, you need to creates rules in GameRules
	*/
public:
	Table();
	Table(const Table &t);
	/*Assesors*/
	vector<Player*>* getPlayers();
	Pile* getPile(int n);
	int getIdPlayer(Player *player);
	vector<ACTION>* getHistory();
	vector<int>* getRankings();
	/*Setters*/
	void sitPlayer(Player *player);
	void addPile(int n=1);
	void addAction(ACTION &action);
	virtual ~Table();
private:
	vector<Player*> players; //list the players
	vector<Pile> decks; //contains the deck on the board
	vector<ACTION> history; //memorise every mouvement
	vector<int> previousRanking; //memorise rank
	vector<int> rankings;
};

#endif /* TABLE_H_ */
