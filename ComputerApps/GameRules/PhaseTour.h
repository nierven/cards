#ifndef PHASETOUR_H_
#define PHASETOUR_H_

#include "Phase.h"
#include "Paquet.h"
#include "PhaseNext.h"
#include "PhaseEnd.h"

class PhaseTour :
    public Phase
{
private:
    ACTION action;
    void updateUI(Table& t);


public:
    void begin(Table& t);
    bool legal(Table& t);
    void exec(Table& t);
    void end(Table& t);

    bool isActionValid(Table& t);
    bool isWinner(Table& t);
};

#endif;