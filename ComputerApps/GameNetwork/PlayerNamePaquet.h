#pragma once
#include "Paquet.h"
#include <vector>

class PlayerNamePaquet : public Paquet
{
private:
	vector<string> _playerName;

public:

	PlayerNamePaquet();
	PlayerNamePaquet(vector<string> playerName, int sizeMsg);
	PlayerNamePaquet(const PlayerNamePaquet& paquet);

	vector<string> getPlayerName() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);

};

