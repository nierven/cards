#include "IDPaquet.h"

IDPaquet::IDPaquet() : Paquet()
{
	this->_idGame = 0;
	this->_idPlayer = 0;
}

IDPaquet::IDPaquet(int idGame, int idPlayer, int sizeMsg) : Paquet(ID, sizeMsg)
{
	this->_idGame = idGame;
	this->_idPlayer = idPlayer;
}

IDPaquet::IDPaquet(const IDPaquet& paquet) : Paquet(paquet)
{
	this->_idGame = paquet._idGame;
	this->_idPlayer = paquet._idPlayer;
}

IDPaquet::IDPaquet(const IDPaquet* paquet) : Paquet(paquet)
{
	this->_idGame = paquet->_idGame;
	this->_idPlayer = paquet->_idPlayer;
}

int IDPaquet::getIDGame() const
{
	return this->_idGame;
}

int IDPaquet::getIDPlayer() const
{
	return this->_idPlayer;
}

string IDPaquet::encode()
{
	string message;

	message = to_string(this->_idGame) + ":" + to_string(this->_idPlayer);
	this->_msgLength = message.length();
	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void IDPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 4)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_idGame = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_idPlayer = stoi(info);
					stateDecode++;
				}
				else {
					i++;
				}
				break;

			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}


