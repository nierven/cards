package com.example.gameversion3;

import java.net.InetAddress;
import java.util.ArrayList;


/**
 * Interface définissant les méthodes communes que doit posséder le Controlleur du jeu de cartes
 * On peut diviser ces méthodes en deux catégories :
 * - les méthodes en lien avec les différentes activités de l'application
 * - les méthodes en lien avec la communication réseau effectuée avec le serveur PC
 */


public interface GameController {


    /**
     * Lien avec les différentes activités
     */

    //
    // Lien avec la classe MainActivity
    //


    //
    // Lien avec la classe StartingGameActivity
    //




}
