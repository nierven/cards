#include "State_Game.h"
#include "Table.h"
#include "PhaseStart.h"
#include <iostream>

void State_Game::onInit()
{
	//GameTable.startGame(); En attente
	
	Phase::CurrentPhase = new PhaseStart;
	Phase::CurrentPhase->setListClient(NetworkServer.getPtrListClist());
}

void State_Game::Loop()
{
	// Attendre une action de la part des joueurs
		// R�soudre l'action
		// Envoyer les paquets r�seaux r�sultants
	Phase::CurrentPhase->play(GameTable);
}

void State_Game::onExit()
{

}