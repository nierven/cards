package com.example.gameversion3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class EndGamePaquet extends Paquet{

    private ArrayList<String> _playerName;

    public EndGamePaquet(){
        super();
    }

    public ArrayList<String> get_playerName() {
        return _playerName;
    }

    public EndGamePaquet(ArrayList<String> playerName){
        super(type_action.END_GAME);
        _playerName = playerName;
    }

    public EndGamePaquet(EndGamePaquet paquet){
        super(paquet);
        _playerName = paquet._playerName;
    }

    @Override
    String encode() {
        String message = Integer.toString(_playerName.size());
        _msgLength = 0;

        for(int i = 0; i<_playerName.size(); i++){
            message += _playerName.get(i);
            if( i != _playerName.size() - 1){
                message += "/";
            }
        }

        _msg = _msgFunction.toString() + ":" + _msgLength + message;
        return _msg;
    }

    @Override
    void decode(String receivedMsg) {

        String[] infoSplited;
        int state = 0;
        int nbPlayer=0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 5) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        nbPlayer = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (!Objects.equals(infoSplited[3], "")) {
                        String[] name = infoSplited[3].split("/");
                        _playerName.addAll(Arrays.asList(name).subList(0, nbPlayer));
                    }
                    break;
            }
        }
    }
}
