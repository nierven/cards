#pragma once
#include "Paquet.h"

class GenericPaquet : public Paquet
{
public:

	GenericPaquet();
	GenericPaquet(type_action action, int sizeMsg);
	GenericPaquet(const GenericPaquet& paquet);

	string encode();
	void decode(string receivedMsg);
};

