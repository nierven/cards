/*
 * Table.cpp
 *
 *  Created on: 8 oct. 2023
 *      Author: lorenzo
 */

#include "Table.h"
#include <iostream>
#include <string>

#include "ClassicDeck.h"
#include "Pile.h"
using namespace std;

Table::Table() {
}

Table::Table(const Table &t) {
	this->decks = t.decks;
	this->history = t.history;
	this->players = t.players;
	this->previousRanking = t.rankings;
}


vector<Player*>* Table::getPlayers(){
	return &this->players;
}

Pile* Table::getPile(int n){
	vector<Pile>::iterator deck = this->decks.begin();
	advance(deck,n);
	return &(*deck);
}

int Table::getIdPlayer(Player* player)
{
	for (int i = 0; i < this->getPlayers()->size(); i++) {
		if ((*this->getPlayers())[i] == player) return i;
	}
	return -1;
}

vector<ACTION>* Table::getHistory(){
	return &this->history;
}

vector<int>* Table::getRankings(){
	return &this->rankings;
}

void Table::sitPlayer(Player *player) {
	this->players.push_back(player);
	this->rankings.push_back(0);
}

void Table::addPile(int n){
	for (int i = 0;i < n; i++) {
		this->decks.push_back(Pile());
	}
}

void Table::addAction(ACTION &action){
	this->history.push_back(action);
}


Table::~Table() {
// TODO Auto-generated destructor stub
}

