#pragma once
#include "Paquet.h"
#include "Card.h"
#include <vector>

class RemoveCardPaquet : public Paquet
{
private:
	vector<Card> _listCard;
	int _origin;

public:

	RemoveCardPaquet();
	RemoveCardPaquet(vector<Card> listCard, int origin, int sizeMsg);
	RemoveCardPaquet(const RemoveCardPaquet& paquet);

	vector<Card> getListCard() const;
	int getOrigin() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);

};

