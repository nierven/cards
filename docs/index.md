# Documentation de Cards

![cards](architecture/cards_view.png)

Cards est un projet logiciel cherchant à développer un jeu vidéo de cartes alliant applications graphiques et interactions dans le monde réel. Nous avons implémenté à notre projet un jeu de [Président](https://www.regles-de-jeux.com/regle-du-president/) et développé un modèle de jeu théoriquement adaptable pour d'autres jeux avec d'autres règles.

Le principe, pour jouer une partie, est de regrouper des joueurs autour d'un écran sur lequel est visible la **Visualization**. Celle-ci permet d'afficher la table de jeu, c'est-à-dire une vue d'ensemble de l'état du jeu tel que le verrait un spectateur. Chaque joueur a besoin d'un smartphone et de l'application **Cards** qui lui permet de visualiser sa main (les cartes qu'il possède) et de jouer en conséquence l'action qu'il souhaite à tout moment du jeu. En parallèle, un PC fait s'éxecuter un serveur de jeu vers lequel les joueurs et la Visualization doivent se connecter pour jouer. Ce serveur est en charge de vérifier les actions prises par chaque joueur et de dicter l'avancée du jeu. Tous les acteurs sont connectés via le même sous-réseau LAN.

## Table des matières

- [Comment jouer une partie ?](tuto.md)
- Gestion de projet
    - [Organisation du projet](gestion_de_projet/organisation.md)
    - [Évolutions envisagées](gestion_de_projet/evolutions.md)
- Documentation technique
    - [Structure du projet](documentation_technique/structure.md)
    - [Application PC](documentation_technique/applications_pc/structure.md)
    - [Application Android](documentation_technique/application_mobile/structure.md)
    - [Compilation des applications](documentation_technique/build.md)