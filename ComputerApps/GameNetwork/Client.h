#pragma once
//#include "Paquet.h"
#include "Player.h"
#include <SFML/Network.hpp>
#include <string>
#include <iostream>
#include <thread>
#include <chrono>
#include <queue>
#include <iterator>
#include <SFML/System/Mutex.hpp>
#include "AllPaquet.h"

using namespace std;
using namespace sf;



class Client
{
private:
	int idPlayer;
	Player player;
	int idGame;
	queue<Paquet*> listReceivedPaquet;
	queue<Paquet*> paquetTosend;
	thread receiveThread;
	thread sendThread;
	thread threadConnection;
	string name;
	TcpSocket* socket;
	Mutex mutex;
	Socket::Status clientStatus;
	IpAddress adress;
	int port;

	void receivePaquet(); //fonction d'ouveture du thread d'�coute
	void sendPaquet(); //fonction d'envoye d'un paquet

	//friend void swap(Client& first, Client& second)
	//{
	//	using std::swap;

	//	swap(first.id, second.id);
	//	//swap()
	//}

public:

	bool isReady = false;

	Client();
	Client(int idGame, int idJoueur, TcpSocket* socket);
	Client(const Client& client);
	Client(IpAddress adress, int port);
	Client(IpAddress adress, int port, string name);

	Player* getPlayer();
	queue<Paquet*>* getListReceivedPaquet(); //liste des Paquets en attente
	string getName();

	void addPaquetToSend(Paquet* paquet); //ajout d'un paquet dans la liste des paquets en attente
	

	void launchConnection();
	void connectFromServer();
	void connectFromClient();
	void close();
	//void pushId(); //envoie de l'indentifiant au serveur

	~Client();
	//inline Client& operator=(Client c);


};

Client* findVisu(vector<Client*>& listClient);
