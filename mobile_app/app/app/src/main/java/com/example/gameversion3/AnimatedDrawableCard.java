package com.example.gameversion3;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AnimatedDrawableCard extends Drawable {
    private final static long ANIMATION_DURATION_IN_MS = 400;
    private final static long ANIMATION_TRANSLATION_UP_DURATION_IN_MS = 400;
    private final static long ANIMATION_TRANSLATION_LEFT_DURATION_IN_MS = 25;
    private final static int shiftCardAnimation = GameActivity.screenHeight/6;
    private Drawable drawable;
    private ValueAnimator translateUpAnimator = null;
    private int currentTopDrawableCard;
    private int currentBottomDrawableCard;
    private int currentLeftDrawableCard;
    private int currentRightDrawableCard;
    private int finalTopDrawableCard;
    static int positionHand;

    public AnimatedDrawableCard(Drawable drawable, int positionHand) {
        this.drawable = drawable;
        this.setBounds(drawable.getBounds());
        AnimatedDrawableCard.positionHand = positionHand;
        this.calculatePositions();
        this.prepareAnimation();
    }

    private void calculatePositions() {
        int quotient = positionHand / GameActivity.nbCardsLine;

        currentTopDrawableCard = -GameActivity.screenHeight/2;
        currentBottomDrawableCard = 0;
        finalTopDrawableCard = GameActivity.screenHeight/2 + ((quotient * GameActivity.screenHeight) / 6);
        currentLeftDrawableCard = drawable.getBounds().left;
        currentRightDrawableCard = drawable.getBounds().right;
    }

    private void prepareAnimation() {
        final ValueAnimator translateAnimator = ValueAnimator.ofInt(-GameActivity.screenHeight/2, finalTopDrawableCard);
        translateAnimator.addUpdateListener(valueAnimator -> {
            currentTopDrawableCard = (int) valueAnimator.getAnimatedValue();
            if (currentTopDrawableCard == finalTopDrawableCard) {
                if (AnimatedDrawableCard.positionHand == GameControllerPresident.nbCards - 1) {
                    GameControllerPresident.distributeHandAnimationIsFinished = true;
                }
            }
            currentBottomDrawableCard = currentTopDrawableCard + GameActivity.screenHeight/2;
            invalidateSelf();
        });

        translateAnimator.setDuration(ANIMATION_DURATION_IN_MS);
        translateAnimator.setInterpolator(new LinearInterpolator());
        long delay = (long) positionHand * 3 * ANIMATION_DURATION_IN_MS / 4;
        translateAnimator.setStartDelay(delay);
        translateAnimator.start();
    }

    public void animateOnCardPlayed(int numberTranslation) {

        int finalValue = currentLeftDrawableCard - numberTranslation*(GameActivity.screenWidth/15);
        if (finalValue < 0) {
            currentBottomDrawableCard -= GameActivity.screenHeight / 6;
            currentTopDrawableCard -= GameActivity.screenHeight / 6;
            finalValue += GameActivity.screenWidth - GameActivity.cardWidth;
        }


        final ValueAnimator translateLeftAnimator = ValueAnimator.ofInt(currentLeftDrawableCard, finalValue);
        translateLeftAnimator.addUpdateListener(valueAnimator -> {
            currentLeftDrawableCard = (int) valueAnimator.getAnimatedValue();
            currentRightDrawableCard = currentLeftDrawableCard + GameActivity.cardWidth;
            invalidateSelf();
        });

        translateLeftAnimator.setDuration(ANIMATION_TRANSLATION_LEFT_DURATION_IN_MS);
        translateLeftAnimator.setInterpolator(new LinearInterpolator());
        translateLeftAnimator.start();
    }

    public void initializeAnimationSelectedCard() {

        translateUpAnimator = ValueAnimator.ofInt(currentTopDrawableCard, currentTopDrawableCard - shiftCardAnimation);

        translateUpAnimator.addUpdateListener(valueAnimator -> {
            currentTopDrawableCard = (int) valueAnimator.getAnimatedValue();
            currentBottomDrawableCard = currentTopDrawableCard + GameActivity.screenHeight/2;
            invalidateSelf();
        });

        translateUpAnimator.setDuration(ANIMATION_TRANSLATION_UP_DURATION_IN_MS);
        translateUpAnimator.setInterpolator(new LinearInterpolator());
    }
    public void animateSelectedCard() {
        if (translateUpAnimator == null) {
            this.initializeAnimationSelectedCard();
        }
        translateUpAnimator.start();

    }
    public void animateUnselectedCard() {
        translateUpAnimator.reverse();
    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        Rect bounds = drawable.copyBounds();

        bounds.top = currentTopDrawableCard;
        bounds.bottom = currentBottomDrawableCard;
        bounds.left = currentLeftDrawableCard;
        bounds.right = currentRightDrawableCard;

        drawable.setBounds(bounds);
        this.setBounds(bounds);

        drawable.draw(canvas);
        GameControllerPresident.StaticController.updateCustomView();
    }



    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

}
