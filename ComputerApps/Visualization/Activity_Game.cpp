#include "Activity_Game.h"
#include "Visualization.h"
#include "Client.h"
#include "Card.h"
#include "Paquet.h"
#include<String>
#include <map>
#include <algorithm>
#include <iterator>

int nb_players = 4;
int nb_cards = 0;
std::vector<std::string> CardNames;

// Les noms des joueurs et le nombre de cartes doivent �tre stock�s dans des tableaux
//std::vector<std::string> player_noms {"X", "Y", "Z", "W"};
std::vector<int> player_nbcartes {8, 8, 8, 8};

const int nbTotalCartes = 54;
map<std::string, sf::Texture*> cardTextures;

const float scaleX = 0.2f;  // Facteur d'�chelle horizontal (x)
const float scaleY = 0.3f;  // Facteur d'�chelle vertical (y)

string ancienPlayerTurn ="";

Activity_Game::Activity_Game(Visualization* visualization, Client* client) : Activity(visualization)
{
    _client = client;

    // Chargement des ressources
    if (!_menuFont.loadFromFile("arial.ttf"))
    {
        exit(-1);
    }

    for (int color = 1; color < 5; color++) // Undefined � 0 donc on commence � 1
    {
        for (int value = 1; value < 14; value++)
        {
            Card tmpCard(color, value);
            string cardName = tmpCard.toString();
            cardTextures[cardName] = new sf::Texture;
            cardTextures[cardName]->loadFromFile("Player Cards/" + cardName + ".png");
        }
    }
}

void Activity_Game::Init()
{
    nb_players = _player_connectes_noms->size();

    // Cr�ation du fond d'�cran
    sf::Texture* backgroundTexture = new Texture;
    if (!backgroundTexture->loadFromFile("Player Cards/background.png")) {}
    sf::Sprite* backgroundSprite = new sf::Sprite;
    backgroundSprite->setTexture(*backgroundTexture);

    float scaleX_background = 1.5f;  // Facteur d'�chelle horizontal (x)
    float scaleY_background = 1.7f;  // Facteur d'�chelle vertical (y)
    backgroundSprite->setScale(scaleX_background, scaleY_background);
    _objects.Add("backgroundSprit", backgroundSprite, -1);

    // Texture du dos de carte de jeu
    sf::Texture* backOfCardTexture = new Texture;

    if (!backOfCardTexture->loadFromFile("Player Cards/back.png")) {}
    // Cartes au centre de la table
    for (int j = 0; j < nb_cards; j++)
    {
        sf::Sprite* CardSprite = new sf::Sprite;
        CardSprite->setTexture(*cardTextures[CardNames[j]]);

        CardSprite->setPosition(300.0f + 50 * j, 200.0f);
        CardSprite->setScale(scaleX, scaleY);
        _objects.Add("CardSprite_" + to_string(j), CardSprite);
    }

    // Cr�ation des piles de cartes pour les joueurs
    for (int player_id = 0; player_id < nb_players; player_id++)
    {
        // Cr�er des textes pour afficher le nom du joueur au-dessus de chaque carte
        sf::Text* PlayerText = new sf::Text;
        PlayerText->setFont(_menuFont);
        PlayerText->setString((*_player_connectes_noms)[player_id]);
        PlayerText->setCharacterSize(16);
        _objects.Add("PlayerText_" + (*_player_connectes_noms)[player_id], PlayerText);

        // Position des cartes sur la fen�tre 
        //Changement par rapport � V1: on affiche toutes les cartes au lieu d'afficher le nb de carte sous forme de texte 
        if (player_id == 0) {
            for (int j = 0; j < player_nbcartes[0];j++) {
                sf::Sprite* PlayerCardSprite = new sf::Sprite;
                PlayerCardSprite->setTexture(*backOfCardTexture);
                PlayerCardSprite->setScale(scaleX, scaleY);
                _objects.Add("PlayerCardSprite_" + to_string(player_id)+ to_string(j), PlayerCardSprite);
                PlayerCardSprite->setPosition(10.0f, 200.0f - player_nbcartes[0] / 2 + 10 * j);
            }
            PlayerText->setPosition(10.0f, 420.0f);
        }
        else if (player_id == 1) {
            for (int j = 0; j < player_nbcartes[1];j++) {
                sf::Sprite* PlayerCardSprite = new sf::Sprite;
                PlayerCardSprite->setTexture(*backOfCardTexture);
                PlayerCardSprite->setScale(scaleX, scaleY);
                _objects.Add("PlayerCardSprite_" + to_string(player_id) + to_string(j), PlayerCardSprite);
                PlayerCardSprite->setPosition(670.0f, 200.0f - player_nbcartes[1] / 2 + 10 * j);
            }
            PlayerText->setPosition(670.0f, 420.0f);
        }
        else if (player_id == 2) {
            for (int j = 0; j < player_nbcartes[2];j++) {
                sf::Sprite* PlayerCardSprite = new sf::Sprite;
                PlayerCardSprite->setTexture(*backOfCardTexture);
                PlayerCardSprite->setScale(scaleX, scaleY);
                _objects.Add("PlayerCardSprite_" + to_string(player_id) + to_string(j), PlayerCardSprite);
                PlayerCardSprite->setPosition(350.0f - player_nbcartes[2] / 2 + 10 * j, 5.0f);
            }
            PlayerText->setPosition(350, 145);
        }
        else if (player_id == 3) {
            for (int j = 0; j < player_nbcartes[3];j++) {
                sf::Sprite* PlayerCardSprite = new sf::Sprite;
                PlayerCardSprite->setTexture(*backOfCardTexture);
                PlayerCardSprite->setScale(scaleX, scaleY);
                _objects.Add("PlayerCardSprite_" + to_string(player_id) + to_string(j), PlayerCardSprite);
                PlayerCardSprite->setPosition(350.0f - (player_nbcartes[3] / 2) + 10 * j, 420.0f);  
            }
            PlayerText->setPosition(350, 560);
        }
    }
}

void Activity_Game::Update()
{
    queue<Paquet*>* dataQueue = _client->getListReceivedPaquet();
    while (!dataQueue->empty())
    {
        // R�cuperation des paquets en attente
        Paquet* paquet = dataQueue->front();
        dataQueue->pop();

        switch (paquet->getMsgFunction())
        {
            case type_action::INFO_ECRAN:
            {
                ScreenInfoPaquet* paquetEcran = (ScreenInfoPaquet*)paquet; //cast
                UpdateUI(paquetEcran);
                break;
            }
            case type_action::PLAYER_TURN:
            {
                PlayerTurnPaquet* paquetTurn = (PlayerTurnPaquet*)paquet; //cast
                UpdateTurn(paquetTurn);
                break;
            }
            case type_action::END_GAME:
            {
                _visualization->SwitchTo(ActivityType::Results);
                break;
            }
        }
    }
}

void Activity_Game::UpdateTurn(PlayerTurnPaquet* paquetTurn) // affichage de qui est cense jouer en rouge 
{   
    string playerTurn = paquetTurn->getPseudo();
    sf::Text* TourText = new sf::Text;
    TourText->setFont(_menuFont);
    TourText->setString("C'est le tour de "+playerTurn);
    TourText->setCharacterSize(16);
    TourText->setPosition(10, 10);
    TourText->setFillColor(sf::Color::Red);
    _objects.Add("PlayerText_" + playerTurn, TourText);

    if (ancienPlayerTurn != "")
    {
        _objects.Remove("PlayerText_" + ancienPlayerTurn);
        sf::Text* PlayerText = new sf::Text;
        PlayerText->setFont(_menuFont);
        PlayerText->setString(ancienPlayerTurn);
        PlayerText->setCharacterSize(16);
        PlayerText->setFillColor(sf::Color::White);
        _objects.Add("PlayerText_" + ancienPlayerTurn, PlayerText);
    }
    

    _objects.Remove("PlayerText_" + playerTurn);
    sf::Text* PlayerText = new sf::Text;
    PlayerText->setFont(_menuFont);
    PlayerText->setString(playerTurn);
    PlayerText->setCharacterSize(16);
    PlayerText->setFillColor(sf::Color::Red);
    _objects.Add("PlayerText_" + playerTurn, PlayerText);
    ancienPlayerTurn = playerTurn;
}
void Activity_Game::UpdateUI(ScreenInfoPaquet* paquetEcran)
{
    int ancienNombreCard = nb_cards;
    std::vector<int >ancienPlayerNbcartes= player_nbcartes;
    // Mise � jour du mod�le du jeu en fonction du paquet
    //cout << "Nb Joueurs " + to_string(paquetEcran->getNbJoueur()) << endl;//size 
    std::vector<int> nbCardPlayer = paquetEcran->getNbCardPlayer();
    nb_players = nbCardPlayer.size();
    player_nbcartes.clear();
    for (int i = 0; i < nb_players; i++)
        player_nbcartes.push_back(nbCardPlayer[i]);

    CardNames.clear();
    std::vector<Card> listCard = paquetEcran->getListCard();
    nb_cards = listCard.size();
    // Pour ne r�cup�rer que les 4 derni�res cartes
    int start_index = max(0, nb_cards - 4);
    for (int i = start_index; i < nb_cards; i++) {
        Card card = listCard[i];
        CardNames.push_back(card.toString());
    }

    // Mise � jour des objets graphiques

    for (int i = 0; i < ancienNombreCard; i++)
    {
        _objects.Remove("CardSprite_" + std::to_string(i));
    }

    for (int i = start_index; i < nb_cards; i++)
    {
        int cardIndex = i - start_index;
        
        sf::Sprite* CardSprite = new sf::Sprite;
        CardSprite->setTexture(*cardTextures[CardNames[cardIndex]]);

        CardSprite->setPosition(300.0f + 50 * cardIndex, 200.0f);
        CardSprite->setScale(scaleX, scaleY);
        _objects.Add("CardSprite_" + to_string(cardIndex), CardSprite);
    }

    for (int player_id = 0; player_id < nb_players; player_id++)
    {
        // Je suppose que le nombre de cartes pour chaque joueur ne fait que diminuer
        if (player_id == 0) {
            for (int j = player_nbcartes[0]; j < ancienPlayerNbcartes[0];j++) {
                _objects.Remove("PlayerCardSprite_" + to_string(player_id) + to_string(j));
            }
        }
        else if (player_id == 1) {
            for (int j = player_nbcartes[1]; j < ancienPlayerNbcartes[1];j++) {
                _objects.Remove("PlayerCardSprite_" + to_string(player_id) + to_string(j));
            }
        }
        else if (player_id == 2) {
            for (int j = player_nbcartes[2]; j < ancienPlayerNbcartes[2];j++) {
                _objects.Remove("PlayerCardSprite_" + to_string(player_id) + to_string(j));
            }
        }
        else if (player_id == 3) {
            for (int j = player_nbcartes[3]; j < ancienPlayerNbcartes[3];j++) {
                _objects.Remove("PlayerCardSprite_" + to_string(player_id) + to_string(j));
            }
        }
    }
}

void Activity_Game::setListNames(vector<string>* listNames)
{
    _player_connectes_noms = listNames;
}
