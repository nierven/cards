#include "PlayerReadyPaquet.h"

PlayerReadyPaquet::PlayerReadyPaquet() : Paquet()
{
	this->_idPlayer = 0;
}

PlayerReadyPaquet::PlayerReadyPaquet(int iDPlayer, int sizeMsg) : Paquet(PLAYER_READY, sizeMsg)
{
	this->_idPlayer = iDPlayer;
}

PlayerReadyPaquet::PlayerReadyPaquet(const PlayerReadyPaquet& paquet) : Paquet(paquet)
{
	this->_idPlayer = paquet._idPlayer;
}

int PlayerReadyPaquet::getIDPlayer() const
{
	return this->_idPlayer;
}

string PlayerReadyPaquet::encode()
{
	string message;

	message = to_string(this->_idPlayer);
	this->_msgLength = message.length();
	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void PlayerReadyPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 3)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_idPlayer = stoi(info);
					stateDecode++;
				}
				else {
					i++;
				}
				break;

			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
