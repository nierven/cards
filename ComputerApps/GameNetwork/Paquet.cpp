#include "Paquet.h"

Paquet::Paquet() {
    this->_msgFunction = RIEN;
    this->_msgLength = 0;
    this->_msg = "";
    this->_sizeMsg = 0;
}

Paquet::Paquet(type_action action, int sizeMsg)
{
    this->_msgFunction = action;
    this->_sizeMsg = sizeMsg;
    //this->_msg = new unsigned char[this->_sizeMsg];
    this->_msg = "";
    this->_msgLength = 0;
}

Paquet::Paquet(const Paquet* paquet)
{
    this->_msgFunction = paquet->_msgFunction;
    this->_sizeMsg = paquet->_sizeMsg;
    this->_msg = paquet->_msg;
    this->_msgLength = paquet->_msgLength;
}

Paquet::Paquet(const Paquet& paquet) {
    this->_msgFunction = paquet._msgFunction;
    this->_sizeMsg = paquet._sizeMsg;
    this->_msgLength = paquet._msgLength;
    this->_msg = paquet._msg;

    /*this->_msg = new unsigned char[this->_sizeMsg];
    for (int i = 0; i < this->_sizeMsg; i++) {
        this->_msg[i] = paquet._msg[i];
    }*/
}

type_action Paquet::getMsgFunction() const
{
    return this->_msgFunction;
}

Paquet::~Paquet() {
    //delete[] this->_msg;
}

string splitString(string chaine, int* i)
{
    string mes = "";

    while ((chaine[*i] != ':') && (chaine[*i] != '\0') && (chaine[*i] != '/')) {
        mes += chaine[*i];
        (*i)++;
    }

    return mes;
}