#include "Activity.h"
#include "Client.h"
class Activity_Results : public Activity
{
public:
	Activity_Results(Visualization* visualization, Client* client);
	void Init();
	void Update();
private:
	Client* _client;
	sf::Font _menuFont;
	void UpdateUI(EndGamePaquet* paquetResultat);
};