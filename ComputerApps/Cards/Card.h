#ifndef CARD_H_
#define CARD_H_

#include<string>
using namespace std;

enum COLOR{Undefined=0, Heart, Spade, Clubs, Diamond, };
enum VALUE{Zero=0, As, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King};

class Card {
public:
	/*Constructor*/
	Card();
	Card(COLOR c, VALUE v);
	Card(int c, int v);
	Card(const Card &c);
	Card(string idCard);

	/*Méthods*/
	void setColor(int c);
	void setColor(COLOR c);
	void setValue(VALUE v);
	void setValue(int v);
	/*Printers*/
	string toString();
	string idCard();
	void print();
	/*Accessors*/
	int getColor();
	int getValue();
	virtual ~Card();
	/*Override*/
	virtual bool operator==(Card &carte2);
	virtual bool operator<(Card &carte2);
private:
	COLOR color;
	VALUE value;
};

#endif /* CARD_H_ */
