#include "ScreenInfoPaquet.h"

ScreenInfoPaquet::ScreenInfoPaquet() : Paquet()
{
	this->_listCard = {};
	this->_nbCardPlayer = {};
}

ScreenInfoPaquet::ScreenInfoPaquet(vector<Card> listCard, vector<int> nbCardPlayer, int sizeMsg) : Paquet(INFO_ECRAN, sizeMsg)
{
	this->_listCard = listCard;
	this->_nbCardPlayer = nbCardPlayer;
}

ScreenInfoPaquet::ScreenInfoPaquet(const ScreenInfoPaquet& paquet) : Paquet(paquet)
{
	this->_listCard = paquet._listCard;
	this->_nbCardPlayer = paquet._nbCardPlayer;
}

vector<Card> ScreenInfoPaquet::getListCard() const
{
	return this->_listCard;
}

vector<int> ScreenInfoPaquet::getNbCardPlayer() const
{
	return this->_nbCardPlayer;
}

string ScreenInfoPaquet::encode()
{
	string message;
	message = to_string(this->_listCard.size()) + ":";

	for (int i = 0; i < this->_listCard.size(); i++) {
		message += this->_listCard[i].idCard();
		if (i != (this->_listCard.size()-1)) {
			message += "/";
		}
	}

	message = message + ":" + to_string(this->_nbCardPlayer.size()) + ":";

	for (int i = 0; i < this->_nbCardPlayer.size(); i++) {
		message += to_string(this->_nbCardPlayer[i]);
		if (i != (this->_nbCardPlayer.size()-1)) {
			message += "/";
		}
	}

	this->_msgLength = message.length();

	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void ScreenInfoPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	int nbCard = 0;
	int nbPlayer = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 6)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					nbCard = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				for (int j = 0; j < nbCard; j++) {
					info = splitString(receivedMsg, &i);
					this->_listCard.push_back(Card(info));
					i++;
				}
				stateDecode++;
				break;
			case(4):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					nbPlayer = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(5):
				for (int j = 0; j < nbPlayer; j++) {
					info = splitString(receivedMsg, &i);
					this->_nbCardPlayer.push_back(stoi(info));
					if(j != nbPlayer-1)
						i++;
				}
				stateDecode++;
				break;
			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}
