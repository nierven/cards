package com.example.gameversion3;

import java.util.Objects;

public class IDPaquet extends Paquet {

    private int _idGame;
    private int _idPlayer;

    public IDPaquet() {
        super();
        this._idGame = 0;
        this._idPlayer = 0;
    }



    public IDPaquet(int idGame, int idPlayer) {
        super(type_action.ID);
        this._idGame = idGame;
        this._idPlayer = idPlayer;
    }

    public IDPaquet(IDPaquet paquet) {
        super(paquet);
        this._idGame = paquet._idGame;
        this._idPlayer = paquet._idPlayer;
    }

    public int get_idGame() {
        return _idGame;
    }

    public int get_idPlayer() {
        return _idPlayer;
    }

    @Override
    String encode() {
        String message;
        message = _idGame + ":" + _idPlayer;
        _msgLength = message.length();
        _msg = _msgFunction.toString() + ":" + _msgLength + ":" + message;
        return _msg;
    }

    @Override
    void decode(String receivedMsg) {

        String[] infoSplited;
        int state = 0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 3) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        _idGame = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (!Objects.equals(infoSplited[3], "")) {
                        _idPlayer = Integer.parseInt(infoSplited[3]);
                    }
                    break;

            }
        }
    }
}
