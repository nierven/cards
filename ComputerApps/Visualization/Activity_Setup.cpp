#include "Activity_Setup.h"
#include "Visualization.h"
#include "Client.h"
#include "Paquet.h"
#include<String>

Activity_Setup::Activity_Setup(Visualization *visualization,Client* client) : Activity(visualization)
{
	_client = client;
	_client->connectFromClient();

	// Chargement des ressources
	if (!_menuFont.loadFromFile("arial.ttf"))
	{
		exit(-1);
	}
}

void Activity_Setup::Init()
{
	// Cr�ation des objets � dessiner
	// ------------------------------

	// Texte d'attente
	sf::Text* attenteText = new sf::Text;
	attenteText->setFont(_menuFont);
	attenteText->setPosition(10, 10);
	attenteText->setCharacterSize(20);
	attenteText->setString("Attente des Joueurs");
	_objects.Add("attenteText", attenteText);
}

void Activity_Setup::Update()
{
	queue<Paquet*>* dataQueue = _client->getListReceivedPaquet();
	while (!dataQueue->empty())
	{
		// R�cuperation des paquets en attente
		Paquet* paquet = dataQueue->front();
		dataQueue->pop();

		switch (paquet->getMsgFunction())
		{
			case type_action::PLAYER_NAME:
			{   
				PlayerNamePaquet* paquetNamePlayer = (PlayerNamePaquet*)paquet;
				UpdateUI(paquetNamePlayer);
				break;
			}
			case type_action::INFO_ECRAN:
			{
				_visualization->SwitchTo(ActivityType::Game);
				break;
			}
		}
	}
}

vector<string>* Activity_Setup::getListNames()
{
	return _player_connectes_noms;
}

void Activity_Setup::UpdateUI(PlayerNamePaquet* paquetNamePlayer)
{ 
	_player_connectes_noms = new vector(paquetNamePlayer->getPlayerName());
	int nb_players_connectes = (*_player_connectes_noms).size();
	for (int i = 0; i < nb_players_connectes; i++)
	{   
		sf::Text* JoueurConnecte = new sf::Text;
		JoueurConnecte->setFont(_menuFont);
		JoueurConnecte->setCharacterSize(20);
		JoueurConnecte->setPosition(100.0f + 50 * i, 100 + 50.0f*i);
		JoueurConnecte->setString((*_player_connectes_noms)[i] + " s'est connect� ");
		_objects.Add("JoueurConnecte_" + to_string(i), JoueurConnecte);
	}
}