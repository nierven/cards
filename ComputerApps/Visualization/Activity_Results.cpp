#include "Activity.h"
#include "Client.h"
#include "Activity_Results.h"
#include "Visualization.h"

vector<string> classement;
Activity_Results::Activity_Results(Visualization* visualization, Client* client) : Activity(visualization)
{
	_client = client;
}

void Activity_Results::Init()
{

}

void Activity_Results::Update()
{
    queue<Paquet*>* dataQueue = _client->getListReceivedPaquet();
    while (!dataQueue->empty())
    {
        // Récuperation des paquets en attente
        Paquet* paquet = dataQueue->front();
        dataQueue->pop();

        switch (paquet->getMsgFunction())
        {
            case type_action::END_GAME:
            {   
                EndGamePaquet* paquetResultat = (EndGamePaquet*)paquet; //cast
                UpdateUI(paquetResultat);
                break;
            }
            case type_action::PLAYER_NAME: // si on recoit un paquet de type PLAYER_NAME on relance une partie
            {
                _visualization->SwitchTo(ActivityType::Setup);
                break;
            }

        }
    }
}

void Activity_Results::UpdateUI(EndGamePaquet* paquetResultat){
	//afficher les results 
    sf::Text* finText = new sf::Text;
    finText->setFont(_menuFont);
    finText->setPosition(10, 10);
    finText->setCharacterSize(18);
    finText->setString("Fin de partie! Voici votre classement");
    _objects.Add("finText", finText);

    classement = paquetResultat->getRanking();
    int nb_players = classement.size();
    for (int i = 0; i < nb_players; i++)
    {
        sf::Text* classementJoueur = new sf::Text;
        classementJoueur->setFont(_menuFont);
        classementJoueur->setCharacterSize(16);
        classementJoueur->setPosition(10.0f + 10 * i, 10.0f);
        classementJoueur->setString(to_string(i)+":"+classement[i]);
        _objects.Add("classementJoueur_" + to_string(i), classementJoueur);
    }
    sf::Text* rejouer = new sf::Text;
    rejouer->setFont(_menuFont);
    rejouer->setPosition(10, 10);
    rejouer->setCharacterSize(18);
    rejouer->setString("Vous pouver rejouer!");
    _objects.Add("rejouer", rejouer);
}