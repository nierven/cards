#include "Activity.h"
#include "Visualization.h"

Activity::Activity(Visualization *visualization)
{
	_visualization = visualization;
	_window = _visualization->getWindow();
}

void Activity::Activate()
{

}
void Activity::Deactivate()
{

}
void Activity::Render()
{
	for (auto const& object : _objects)
	{
		_window->draw(*object.drawable);
	}
}