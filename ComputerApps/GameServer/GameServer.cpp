#include "ServerState.h"
#include "State_Setup.h"
#include "State_Game.h"
#include "State_Results.h"
#include "State_Error.h"

#include "Phase.h"
#include "PhaseStart.h"	
#include "PhaseTour.h"

#include <chrono>
#include <thread>
#include <map>
#include "Serveur.h"
#include "Table.h"
#include "Globals.h"

using namespace std;

const int Framerate = 120;
std::chrono::duration<double> FrameTime(std::chrono::milliseconds((long) (1000.0 / Framerate)));

Table GameTable;
Serveur NetworkServer(1);

// Initilialisation des phases de jeu possibles
ServerStates CurrentState = ServerStates::None;
Phase* Phase::CurrentPhase = NULL;

int main()
{
	bool isDone = false;
	chrono::time_point<std::chrono::system_clock> start_frame_time, end_frame_time;

	// Initialisation des �tats r�alisables du serveur
	ServerStates previousState = ServerStates::None;
	map<ServerStates, ServerState*> states
	{
		{ ServerStates::Setup, new State_Setup },
		{ ServerStates::Game, new State_Game },
		{ ServerStates::Results, new State_Results },
		{ ServerStates::Error, new State_Error },
	};

	// Boucle principale
	while (!isDone)
	{
		start_frame_time = chrono::system_clock::now();

		// Machine � �tat globale
		switch (CurrentState)
		{
			case ServerStates::None:
				// Si aucun �tat, lancer le setup du jeu
				CurrentState = ServerStates::Setup;
				break;
			default:
				// Sinon effectuer la logique de l'�tat actuel + onExit et onInit lors d'un changement d'�tat
				if (previousState != CurrentState)
				{
					if (previousState != ServerStates::None) states[previousState]->onExit();
					states[CurrentState]->onInit();
					previousState = CurrentState;
				}

				states[CurrentState]->Loop();
				break;
		}

		// Contr�le du framerate
		end_frame_time = chrono::system_clock::now();
		std::chrono::duration<double> elapsedTime = end_frame_time - start_frame_time;

		if (elapsedTime < FrameTime)
		{
			// Mise en pause du thread pour le temps restant de la frame
			this_thread::sleep_for(FrameTime - elapsedTime);
		}
	}
	NetworkServer.close();
	return 0;
}