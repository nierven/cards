#pragma once

enum ServerStates
{
	None,
	Setup,
	Game,
	Results,
	Error
};

class ServerState
{
public:
	virtual void onInit() {}
	virtual void Loop() = 0;
	virtual void onExit() {}
};