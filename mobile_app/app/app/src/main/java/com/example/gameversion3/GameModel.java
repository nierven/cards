package com.example.gameversion3;

import java.util.ArrayList;

/**
 * Ne consiste qu'en un ensemble de cartes.
 * Ne contient qu'un getter, un setter et un constructeur par défaut
 */
public class GameModel {

    // Variable Hand contenant la liste des cartes appartenant au joueur
    private ArrayList<Card> hand;

    public GameModel() {
        this.hand = new ArrayList<>();
    }
    public ArrayList<Card> getHand() {
        return hand;
    }
    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }
}
