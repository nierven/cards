package com.example.gameversion3;

import java.util.Objects;

public class AskCardPaquet extends Paquet{

    private int _numberCards;

    public AskCardPaquet(){
        super();
        _numberCards = 0;
    }

    public AskCardPaquet(int numberCards){
        super(type_action.ASK);
        _numberCards = numberCards;
    }

    public AskCardPaquet(AskCardPaquet paquet){
        super(paquet);
        _numberCards = paquet._numberCards;
    }

    public int get_numberCards(){
        return _numberCards;
    }

    @Override
    public String encode() {
        String message;
        message = Integer.toString(_numberCards);
        _msgLength = message.length();
        _msg = _msgFunction.toString() + ":" + _msgLength + ":" + message;
        return _msg;
    }

    @Override
    public void decode(String receivedMsg) {
        String[] infoSplited;
        int state = 0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 3) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        _numberCards = Integer.parseInt(infoSplited[2]);
                    }
                    break;
            }
        }
    }
}
