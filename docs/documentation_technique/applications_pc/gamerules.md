# GameRules

Liste des enums :

- **PhaseState** (Phase.h) : État d'une phase abstraite

Liste des classes :

- **Phase** (.h) : Classe abstraite permettant de définir une phase de jeu. Une phase est gérée par une machine à état simple permettant d'utiliser une fonction initiale, une fonction de loop et une fonction de fin de phase
    - **PhaseNext** (PhaseNext.h) : Phase de passage entre le tour d'un joueur et d'un autre
    - **PhaseStart** (PhaseStart.h) : Phase de démarrage du jeu, distribution des cartes et mise en place de la visualisation
    - **PhaseTour** (PhaseTour.h) : Phase de tour, possibilité pour tous les joueurs d'envoyer un message, pour lequel le serveur vérifiera la validité à l'aide de la fonction `isActionValid()`