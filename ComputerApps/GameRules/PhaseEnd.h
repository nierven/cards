#ifndef PHASEEND_H_
#define PHASEEND_H_

#include "Phase.h"
#include "Paquet.h"

class PhaseEnd :
    public Phase
{
private:
    ACTION action;
    void updateUI(Table& t);


public:
    void begin(Table& t);
    bool legal(Table& t);
    void exec(Table& t);
    void end(Table& t);

    bool isActionValid(Table& t);
};

#endif;