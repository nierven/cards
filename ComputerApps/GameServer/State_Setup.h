#pragma once

#include "ServerState.h"
#include "Globals.h"

class State_Setup : public ServerState
{
public:
	void onInit();
	void onExit();
	void Loop();
};