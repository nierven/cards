#pragma once
#include "Paquet.h"
#include "Card.h"
#include <vector>
#include <string>

class PlayCardPaquet : public Paquet 
{
private:
	vector<Card> _listCard;
	int _origin;
	int _destination;

public:
	
	PlayCardPaquet();
	PlayCardPaquet(vector<Card> listCard, int origin, int destination, int sizeMsg);
	PlayCardPaquet(const PlayCardPaquet& paquet);

	vector<Card> getListCard() const;
	int getOrigin() const;
	int getDestination() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);

	~PlayCardPaquet();

};

