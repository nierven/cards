#pragma once
#include "Activity.h"
#include "Client.h"

class Activity_Game : public Activity
{
public:
	Activity_Game(Visualization* visualization, Client* client);
	void Init();
	void Update();

	void setListNames(vector<string>* listNames);

private:
	Client* _client;
	sf::Font _menuFont;

	void UpdateUI(ScreenInfoPaquet* paquetEcran);
	void UpdateTurn(PlayerTurnPaquet* paquetTurn);
};
