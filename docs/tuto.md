# Comment jouer une partie ?

Pour jouer une partie de président, vous aurez besoin de :

- 2 à 4 smartphones Android 
- 1 ou 2 machine(s) Windows

Il vous faudra compiler une première fois le projet global. Réferez-vous à la page [Organisation](gestion_de_projet/organisation.md) pour repérer la version qui vous intéresse et la page [Compilation des applications](documentation_technique/build.md) pour les étapes de compilation.

Lancez les applications sur les smartphones et les PC. L'ordre de lancement est supposément sans impact sur la phase de connexion. Sur les applications mobiles, renseignez votre nom puis appuyez sur `Continuer`. La connexion au serveur de jeu s'effectue à ce moment-là. Si l'application a trouvé un serveur, vous pouvez appuyez sur `Débuter la partie` ce qui enverra un message au serveur pour indiquer que vous êtes prêt. Dès que tous les joueurs connectés sont prêts, le serveur lance la partie.

Le reste devrait être intuitif !