#pragma once
#include "Paquet.h"

class PseudoClientPaquet : public Paquet
{
private:
	string _pseudo;
	int _idClient;

public: 

	PseudoClientPaquet();
	PseudoClientPaquet(string pseudo, int idClient, int sizeMsg);
	PseudoClientPaquet(const PseudoClientPaquet& paquet);

	string getPseudo() const;
	int getIDClient() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);
};

