# Cards - Jeux de cartes multijoueur

## Documentation du projet

[MkDocs](https://nierven.gitlab.io/cards)

## Release actuelle

[v2](https://gitlab.com/nierven/cards/-/releases/v2)

## Membres de l'équipe

- Rachida Saroui
- Quentin Rousset
- Lorenzo Dunau
- Kévin Hoarau
- Gilles-arthur Fade
- Martin Raynaud