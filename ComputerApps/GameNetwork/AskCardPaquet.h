#pragma once
#include "Paquet.h"

class AskCardPaquet :   public Paquet
{
private:
	int _numberCards;

public:

	AskCardPaquet();
	AskCardPaquet(int numberCards, int sizeMsg);
	AskCardPaquet(const AskCardPaquet& paquet);

	int getNumberCards() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);
};

