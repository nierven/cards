# Visualization

La Visualization utilise des activités au même sens qu'une activité Android. Ces activités ont chacune des fonctions de mise à jour logique, dessin graphique et réactivité aux événements. Elles possèdent aussi une liste d'objets graphiques `DrawableObject` qui sont utilisés pour sauvegarder les objets à afficher lorsqu'une activité est mise en avant.