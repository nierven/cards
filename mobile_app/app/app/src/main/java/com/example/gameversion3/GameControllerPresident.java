package com.example.gameversion3;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.MotionEvent;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/***
 * Tour de controle de l'application. Détermine ce que l'on doit faire en cas d'un appui sur l'écran
 * Détermine également ce qu'il doit se passer quand on recoit des données de la
 * part du serveur ou quelles sont les données à envoyer à celui-ci (pas encore implémenté)
 */
public class GameControllerPresident implements GameController{

    /**
     * Liste des attributs de la classe
     */

    //
    // Variables locales
    //

    private StartingGameActivity startingGameActivity;
    private Client client;
    private GameModel gameModel;
    private GameActivity gameActivity = null;
    private GameFinishedActivity gameFinishedActivity = null;
    private int idGame;
    private int idPlayer;
    private String pseudo;
    private boolean playerTurn = false;

    //
    // Variables statiques
    //

    public static boolean distributeHandAnimationIsFinished = false;
    public static GameControllerPresident StaticController;
    public static String IP_ADRESS = null; //
    public final static int PORT_UDP = 2001;
    public final static int PORT_TCP = 2000;
    public static int nbCards;
    public static InetAddress broadcastAddress;
    public static int nbGameWon;
    public final static String BOOLEAN_WIN_CODE = "BOOLEAN_WIN_CODE";
    public static boolean isResumed = false;
    public static ArrayList<Integer> listNumberTranslationCards = new ArrayList<>();

    /**
     *
     * Méthodes de la classe GameControllerPresident
     */

    public GameControllerPresident(StartingGameActivity startingGameActivityView) {
        this.startingGameActivity = startingGameActivityView;
    }


    public void addGameActivity(GameActivity gameActivity, GameModel gameModel) {
        this.gameActivity = gameActivity;
        this.gameModel = gameModel;
    }

    public void addGameFinishedActivity(GameFinishedActivity gameFinishedActivity) {
        this.gameFinishedActivity = gameFinishedActivity;
        this.gameActivity = null;
        this.gameModel = null;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void updateCustomView() {
        this.gameActivity.updateCustomView();
    }

    public void distributeHand(ArrayList<Card> hand) {
        gameActivity.playSoundDistributeCard();
        nbCards = hand.size();
        Collections.sort(hand);
        gameModel.setHand(hand);

        gameActivity.runOnUiThread(() -> gameActivity.distributeHand(hand));
    }

    public void onButtonDrawClick() {
//        ArrayList<Card> hand;
//        hand = this.gameModel.getHand();
//
//        if (hand.size() < 30) {
//            this.gameActivity.playSoundDrawCard();
//            Card.Couleur couleur = Card.Couleur.getRandomCouleur();
//            Card.Valeur valeur = Card.Valeur.getRandomValeur();
//            Card card = new Card(couleur, valeur);
//            hand.add(card);
        // this.gameModel.setHand(hand);
        // gameActivity.addCardToView();
//    }
    }


    public void onButtonPlayClick() {
        ArrayList<Card> hand = gameModel.getHand();
        ArrayList<Card> cardsPlayed = new ArrayList<>();
        for (int i=0; i< hand.size(); i++) {
            if (hand.get(i).getState()) {
                cardsPlayed.add(hand.get(i));
            }
        }

        if (cardsPlayed.size() > 0) {
            Paquet paquetCardPlayed = new PlayCardPaquet(cardsPlayed, idPlayer, 0);
            String paquetToSend = paquetCardPlayed.encode();
            sendMessage(paquetToSend);
        }
    }

    public boolean onHandClick(MotionEvent motionEvent) {
        boolean treated = false;

        int event = motionEvent.getAction();
        if (distributeHandAnimationIsFinished) {
            if (event == MotionEvent.ACTION_DOWN) {
                treated = true;
                int indice = this.getIndiceCardClicked(motionEvent);
                if (indice != 100) {
                    this.animateCardSelected(indice);
                }
            }
        }
        return treated;
    }

    public void animateCardSelected(int indice) {

        ArrayList<Card> hand = this.gameModel.getHand();
        AnimatedDrawableCard animatedDrawableCard = gameActivity.getAnimatedDrawableCardList().get(indice);

        if (hand.get(indice).getState()) {
            animatedDrawableCard.animateSelectedCard();
        }
        else {
            animatedDrawableCard.animateUnselectedCard();
        }
    }

    public int getIndiceCardClicked(MotionEvent motionEvent) {

        LayerDrawable layerDrawable = gameActivity.getLayerDrawable();
        int numberOfLayers = layerDrawable.getNumberOfLayers();
        ArrayList<Integer> indices = new ArrayList<>();

        for (int i=0; i<numberOfLayers; i++) {
            Drawable drawable = layerDrawable.getDrawable(i);
            Rect bounds = drawable.getBounds();
            if (bounds.contains((int) motionEvent.getX(), (int) (motionEvent.getY()))) {
                indices.add(i);
            }
        }
        int max;

        if (indices.size() > 0) {
            max = indices.get(indices.size() - 1);
            ArrayList<Card> hand = this.gameModel.getHand();
            Card card = hand.get(max);
            card.setState(!card.getState());
            if (card.getState()) {
                gameActivity.enablePlayButton();
            }
            hand.set(max, card);
            this.gameModel.setHand(hand);
        }
        else {
            max = 100;
        }

        return max;
    }

    public void removeCards(ArrayList<Card> listCards) {
        ArrayList<Card> hand = gameModel.getHand();

        for (int i=0; i<listCards.size(); i++) {
            for (int j=0; j<hand.size(); j++) {
                if ((listCards.get(i).getCouleur() == hand.get(j).getCouleur()) && (listCards.get(i).getValeur() == hand.get(j).getValeur())) {
                    gameActivity.playSoundPlayCard();
                    nbCards -= 1;
                    final int indice = j;
                    gameActivity.runOnUiThread(() -> gameActivity.removeCardFromCustomView(indice));
                    hand.remove(j);
                    break;
                }
            }
        }

        gameActivity.runOnUiThread(() -> gameActivity.translateCards());


        gameModel.setHand(hand);

    }


    /**
     * Partie communication avec le serveur
      */
    public void connectServer() {
        this.client = new Client();
    }

    public void printReceivedMessage(String message) {
        startingGameActivity.runOnUiThread(() -> startingGameActivity.printReceivedMessage(message));
    }

    public void sendMessage(String message) {
        client.sendMessage(message);
    }

    public boolean analyseDataReceived(String message) {

        Paquet paquet = new GenericPaquet();
        paquet.decode(message);
        boolean beforeDistrib = false;

        switch (paquet.get_msgFunction()) {
            case ID:
                IDPaquet idPaquet = new IDPaquet();
                idPaquet.decode(message);
                idGame = idPaquet.get_idGame();
                idPlayer = idPaquet.get_idPlayer();
                PseudoClientPaquet paquetPseudo = new PseudoClientPaquet(pseudo, idPlayer);
                sendMessage(paquetPseudo.encode());
                startingGameActivity.setGameReady();
                beforeDistrib = true;
                break;
            case DISTRIB:
                if (GameControllerPresident.isResumed) {


                    DistribCardPaquet distribCardPaquet = new DistribCardPaquet();

                    distribCardPaquet.decode(message);

                    ArrayList<Card> hand;
                    hand = distribCardPaquet.get_listCard();
                    distributeHand(hand);
                }
                break;
            case REMOVE:
                if (GameControllerPresident.isResumed) {
                    RemoveCardPaquet removeCardPaquet = new RemoveCardPaquet();
                    removeCardPaquet.decode(message);
                    ArrayList<Card> listCardsPlayed = removeCardPaquet.get_listCard();
                    removeCards(listCardsPlayed);
                }
                break;
            case START:
                startingGameActivity.startGame();
                beforeDistrib = true;
                break;
            case END_GAME:
                EndGamePaquet endGamePaquet = new EndGamePaquet();
                endGamePaquet.decode(message);
                ArrayList<String> listName = endGamePaquet.get_playerName();
                gameActivity.gameFinished(Objects.equals(listName.get(0), pseudo));
                break;
            case COUP_ILLEGAL:
                if (GameControllerPresident.isResumed) {
                    gameActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gameActivity.printReceivedMessage("Pas votre tour");
                        }
                    });
                }
                break;
            default:
                System.out.print("Pas d'action identifiée");
        }
        return beforeDistrib;
    }

    public void sendReadyToServer() {
        GenericPaquet paquet = new GenericPaquet(Paquet.type_action.READY);
        client.sendMessage(paquet.encode());
    }


}
