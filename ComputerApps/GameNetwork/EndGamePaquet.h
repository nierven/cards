#pragma once
#include "Paquet.h"
#include <vector>

class EndGamePaquet : public Paquet
{
private:
	vector<string> _playerName;

public:

	EndGamePaquet();
	EndGamePaquet(vector<string> playerName, int size);
	EndGamePaquet(const EndGamePaquet& paquet);

	vector<string> getRanking() const;

	//unsigned char* encode();
	string encode();
	void decode(string receivedMsg);
};

