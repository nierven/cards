package com.example.gameversion3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.nio.ByteOrder;
import java.util.Objects;

public class StartingGameActivity extends AppCompatActivity {

    private Button button_ready;
    private TextView infoTxt;
    private String pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_game);

        button_ready = findViewById(R.id.activity_starting_game_ready_btn);
        infoTxt = findViewById(R.id.activity_starting_game_info_txt);
        TextView pseudo_txt = findViewById(R.id.activity_starting_game_pseudo_txt);

        button_ready.setEnabled(false);

        pseudo = getIntent().getStringExtra(MainActivity.PSEUDO_CODE);

        pseudo_txt.append(pseudo);

        GameControllerPresident.StaticController = new GameControllerPresident(this);

        GameControllerPresident.StaticController.setPseudo(pseudo);

        try {
            WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            DhcpInfo dhcp = wifi.getDhcpInfo();
            int ipAddress = dhcp.ipAddress;
            try {
                if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                    ipAddress = Integer.reverseBytes(ipAddress);
                }

                byte[] ipByteArrray = BigInteger.valueOf(ipAddress).toByteArray();

                InetAddress inetAddress = InetAddress.getByAddress(ipByteArrray);
                NetworkInterface networkInterface = NetworkInterface.getByInetAddress(inetAddress);
                for (InterfaceAddress address : networkInterface.getInterfaceAddresses()) {
                    GameControllerPresident.broadcastAddress = address.getBroadcast();
                }
            } catch (IOException e) {
                Log.e("netmask", Objects.requireNonNull(e.getMessage()));
            }
        }
        catch (RuntimeException e) {
            GameControllerPresident.broadcastAddress = null;
            Toast.makeText(getBaseContext(),"Pas de connexion wifi", Toast.LENGTH_SHORT).show();
            Intent mainActivity = new Intent(StartingGameActivity.this, MainActivity.class);
            startActivity(mainActivity);
        }

        if (GameControllerPresident.broadcastAddress != null) {
            GameControllerPresident.StaticController.connectServer();
        }


        button_ready.setOnClickListener(view ->
                GameControllerPresident.StaticController.sendReadyToServer()
                //startGame()
        );

    }


    public void startGame() {
        this.runOnUiThread(() -> {
            Intent gameActivity = new Intent(StartingGameActivity.this, GameActivity.class);
            gameActivity.putExtra(MainActivity.PSEUDO_CODE, pseudo);
            startActivity(gameActivity);
        });

    }

    public void setGameReady() {

        this.runOnUiThread(() -> button_ready.setEnabled(true));
    }

    public void printReceivedMessage(String message) {
        infoTxt.setText(message);
    }
}