# Compilation des applications

Pour compiler les applications PC :

- Installez [Visual Studio 2022](https://visualstudio.microsoft.com/downloads/)
- Installez le pack d'extension **Desktop Development with C++**
    - Normalement il vous est proposé à l'installation
    - Sinon allez dans `Tools -> Get Tools and Features`
- Ouvrez la solution `ComputerApps/ComputerApps.sln`
- Compilez les projets **Apps/GameServer** et **Apps/Visualization**
    - Astuce : clic-droit sur la solution pour aller dans les propriétés permet de définir plusieurs projets de démarrage, utile pour le débogage plus tard

Où trouver les applications une fois la compilation terminée (par défaut avec une architecture x64 et le mode débug activé) :

- Visualization : `ComputerApps/x64/Debug/Visualization.exe`
- GameServer : `ComputerApps/x64/Debug/GameServer.exe`

Vous pouvez maintenant lancer ces deux applications sur le même PC ou deux PC différents. Tant qu'ils sont capables de se voir tous deux dans le même sous-réseau.

Il faut à présent compiler et installer les applications Android. Il existe une version pré-compilée copiée à la main dans `mobile_app/app-debug.apk` si vous voulez passer cette partie. Sinon :

- Installez [Android Studio](https://developer.android.com/studio)
- Installez au minimum le SDK v22
- Ouvrez le projet `mobile_app/app/app`
- Compilez l'application et récupérez l'APK (`mobile_app/app/app/...`)