#pragma once
#include<SFML/Network.hpp>//does not exist
#include"Client.h"
#include<map>
#include<iostream>
#include<string>
#include<vector>
#include<SFML/System/Mutex.hpp>

using namespace sf;
using namespace std;

class Serveur
{
private:

	int idGame;
	int nbClient;
	vector<TcpSocket*> listSocket;
	vector<Client*> listClient;
	map<string, int> idClient;
	Mutex mutex;

public:

	Serveur();
	Serveur(int idGame); //Potentiellement suffisant pour lancer un serveur

	int getIdGame(); //utile pour les clients
	int getnbReadyClient(); //Utile?
	int getnbClient(); //Utile? Ouiiiiiiii (sign� Lorenzo)
	vector<Client*>* getPtrListClist() { return &this->listClient; };
	Client* getClient(string name); //R�cup�rer un client � partir de son nom
	map<string, int> * getMapIdClient(); //L'intelligence du jeu peut le r�cuperer au besoin

	void listenConnection(); //methode � lancer dans un thread pour lancer la connexion aux clients 
	void addIdClient(string name, int idClient); //utile pour que les clients push leurs identifiants	
	void broadcastUdp();
	void close();
};

