# Organisation

Ce projet a été réalisé par six membres du M2FESup Intranet en 2023. Il a été supervisé par deux professeurs de l'ENS, Thomas Chatain et Thomas Rodet, et organisé dans le cadre du cours **Génie logiciel**.

## Participants

| Nom | Rôle |
| ----------- | ----------- |
| Quentin Rousset | Chef de projet |
| Rachida Saroui | Développement logiciel (Windows) |
| Martin Raynaud | Développement logiciel (Android) |
| Gilles-Arthur Fade | Conception d'un moteur de jeu |
| Lorenzo Dunau | Conception d'un moteur de jeu |
| Kévin Hoarau | Interfaçage réseau |

## Historique

| Date | Échelon | Détails |
| ----------- | ----------- | ----------- |
| 20/09 |  | Constitution des groupes |
| 27/09 |  | Début du développement, appropriation des technologies |
| 18/10 | v0 | Rassemblement des différents projets en un projet global |
| 22/11 | [v1](https://gitlab.com/nierven/cards/-/releases/v1) | Première version semi-fonctionelle |
| 13/12 | [v2](https://gitlab.com/nierven/cards/-/releases/v2) | Version complète, Échange des projets |
| 10/01 | v3 | Version finale (bonne chance :)) |