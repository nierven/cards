#include "Serveur.h"

Serveur::Serveur()
{
	this->idGame = 0;
	this->nbClient = 0;
	this->idClient = {};
}

Serveur::Serveur(int idGame)
{	
	this->idGame = idGame;
	this->idClient = {};
}

int Serveur::getIdGame()
{
	return this->idGame;
}

int Serveur::getnbClient()
{
	return this->listClient.size();
}

int Serveur::getnbReadyClient()
{
	int nb = 0;
	for (auto const& c : listClient)
		if (c->isReady)
			nb++;

	return nb;
}

Client* Serveur::getClient(string name)
{	
	//int id = idClient[name];
	//return this->listClient[id];

	for (auto & c : listClient)
	{
		if (c->getName() == name)
			return c;
	}

	return NULL;
}

map<string, int>* Serveur::getMapIdClient()
{
	return &this->idClient;
}

void Serveur::listenConnection()
{	
	int i = 0;

	while (true)
	{
		// Initialisation du serveur, ajout d'un Socket si aucun present
		// Potentiellement d�placer cette section dans une fonction � part
		// ou le constructeur (on pr�f�rerait une fonction parce que on peut reset le serveur)
		listSocket.push_back(new TcpSocket);

		TcpListener listener;
		if (listener.listen(2000) != Socket::Done)
		{
			cout << "Listen non lance" << endl;
		}

		cout << "Listening" << endl;
		listener.accept(*listSocket[i]); //methode bloquante pour accepter les connexions 

		this->mutex.lock();
		cout << "Client connecte" << endl;

		this->listClient.push_back(new Client(this->idGame, i, this->listSocket[i])); //Cr�ation d'un client
		this->mutex.unlock();

		new thread(&Client::connectFromServer, this->listClient[i]); //Lancement du thread de communication
		i++;
	}
}

void Serveur::addIdClient(string name, int idClient)
{
	this->mutex.lock();

	this->idClient[name] = idClient; //ajout de d'un identifiant dans la map

	this->mutex.unlock();
}

void Serveur::broadcastUdp()
{
	UdpSocket socket;
	IpAddress localAdress = IpAddress::getLocalAddress();
	Uint32 locAdd = localAdress.toInteger();

	Uint8 addrbroadcast[4];

	addrbroadcast[0] = 0xFF;
	addrbroadcast[1] = (locAdd >> 8) & 0xFF;
	addrbroadcast[2] = (locAdd >> 16) & 0xFF;
	addrbroadcast[3] = (locAdd >> 24) & 0xFF;

	IpAddress broadcast(addrbroadcast[3], addrbroadcast[2], addrbroadcast[1], addrbroadcast[0]);

	char data[100] = "hello";
	unsigned short port = 2001;
	cout << "Lancement connexion UDP : " << broadcast << endl;

	while (1) {
		socket.send(data, 100, broadcast, port);
	}

}

void Serveur::close()
{
	for (int i = 0; i < this->listClient.size(); i++) {
		if(this->listClient[i] != NULL)
			this->listClient[i]->close();
	}
}
