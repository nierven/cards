/*
 * PaquetCartes.cpp
 *
 *  Created on: 7 oct. 2023
 *      Author: lorenzo
 */

#include <cstdlib>
#include <time.h>
#include "ClassicDeck.h"

ClassicDeck::ClassicDeck():Pile(){
	Card buffer;
	for(int color=1; color<5; color++){
		for(int value=1; value<14; value++){
			buffer.setColor(color);
			buffer.setValue(value);
			this->addCard(buffer);
		}
	}
}


ClassicDeck::~ClassicDeck() {
	// TODO Auto-generated destructor stub
}

