#pragma once

#include "ServerState.h"
#include "Globals.h"

class State_Game : public ServerState
{
	void onInit();
	void Loop();
	void onExit();
};