# Évolutions sugérées

Nous avons réfléchi à plusieurs évolutions qui seraient intéressantes pour le projet. Certaines ont commencé à être développées mais le travail n'est pas fini.

- IA de jeu se connectant comme nouveau client au serveur de jeu
- Nouveau jeu de règles : Bataille corse, où l'on peut frapper la table pendant le jeu
- Robustesse des algorithmes de jeu
- Robustesse du réseau aux déconnexions d'un client ou d'une disparition temporaire du serveur