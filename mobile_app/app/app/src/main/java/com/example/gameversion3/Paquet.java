package com.example.gameversion3;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/*
public class Paquet {

    public enum type_action {
        RIEN, ID, DISTRIB, ASK, ATTENTE, REMOVE, SKIP, PLAY, PIOCHE, INFO_IA, INFO_ECRAN, READY, CLIENT_NAME;

        private final int value;

        private type_action() {
            this.value = ordinal();
        }
    }

 */

    /**
     * Attributs de la classe
     */
/*
    private type_action action;
    private int nb_carte;
    private ArrayList<Card> liste_carte; //Liste des cartes transmises dans le paquet
    private int id_pioche;
    private String message_info;
    private int id_joueur;
    private int nb_joueur;
    private ArrayList<Integer> nb_carte_joueur; //Nombre de cartes dans la main de chaque joueur
    private int id_jeu;
    private int id_coup;
 */
    /**
     * Constructeurs
     */
 /*   public Paquet() {
        action = type_action.RIEN;
        nb_carte = 0;
        liste_carte = new ArrayList<>();
        id_pioche = 0;
        message_info = "";
        id_joueur = 0;
        nb_joueur = 0;
        nb_carte_joueur = new ArrayList<>();
        id_jeu = 0;
        id_coup = 0;
    }

    public Paquet(type_action action, int nb_carte, ArrayList<Card> liste_carte, int id_pioche, String message) {
        this.action = action;
        this.nb_carte = nb_carte;
        this.liste_carte = liste_carte;

        this.id_pioche = id_pioche;
        this.message_info = message;
        this.id_jeu = 0;
        this.id_coup = 0;
        this.id_joueur = 0;
        this.nb_joueur = 0;
        this.nb_carte_joueur = new ArrayList<>();
    }

    public Paquet(type_action action, int nb_carte, ArrayList<Card> liste_carte, int nb_joueur, ArrayList<Integer> nb_carte_joueur) {
        this.action = action;
        this.nb_carte = nb_carte;
        this.liste_carte = liste_carte;

        this.id_pioche = 0;
        this.message_info = "";
        this.id_jeu = 0;
        this.id_coup = 0;
        this.id_joueur = 0;
        this.nb_joueur = nb_joueur;
        this.nb_carte_joueur = nb_carte_joueur;
    }

    public Paquet(type_action action) {
        this.action = action;
        this.nb_carte = 0;
        this.liste_carte = new ArrayList<>();
        this.id_pioche = 0;
        this.message_info = "";
        this.id_jeu = 0;
        this.id_coup = 0;
        this.id_joueur = 0;
        this.nb_joueur = 0;
        this.nb_carte_joueur = new ArrayList<>();
    }

    public Paquet(type_action action, String message) {
        this.action = action;
        this.nb_carte = 0;
        this.liste_carte = new ArrayList<>();
        this.id_pioche = 0;
        this.message_info = message;
        this.id_jeu = 0;
        this.id_coup = 0;
        this.id_joueur = 0;
        this.nb_joueur = 0;
        this.nb_carte_joueur = new ArrayList<>();
    }

    public Paquet(type_action action, int id_jeu, int id_joueur) {
        this.action = action;
        nb_carte = 0;
        liste_carte = new ArrayList<>();
        id_pioche = 0;
        message_info = "";
        this.id_joueur = id_joueur;
        nb_joueur = 0;
        nb_carte_joueur = new ArrayList<>();
        this.id_jeu = id_jeu;
        id_coup = 0;
    }

    public Paquet(type_action action, int nb_carte, ArrayList<Card> liste_carte, int id_joueur, int id_pioche, int id_coup) {
        this.action = action;
        this.nb_carte = nb_carte;
        this.liste_carte = liste_carte;

        this.id_pioche = id_pioche;
        this.message_info = "";
        this.id_jeu = 0;
        this.id_coup = id_coup;
        this.id_joueur = id_joueur;
        this.nb_joueur = 0;
        this.nb_carte_joueur = new ArrayList<>();
    }

    public Paquet(Paquet paquet) {
        this.action = paquet.action;
        this.nb_carte = paquet.nb_carte;
        this.liste_carte = new ArrayList<>(paquet.liste_carte);

        this.id_pioche = paquet.id_pioche;
        this.message_info = paquet.message_info;
        this.id_jeu = paquet.id_jeu;
        this.id_coup = paquet.id_coup;
        this.id_joueur = paquet.id_joueur;
        this.nb_joueur = paquet.nb_joueur;
        this.nb_carte_joueur = new ArrayList<>(paquet.nb_carte_joueur);
    }

  */

    /**
     * Getters
     */
/*
    public type_action getAction() {
        return action;
    }

    public int getNb_carte() {
        return nb_carte;
    }

    public ArrayList<Card> getListe_carte() {
        return liste_carte;
    }

    public int getId_pioche() {
        return id_pioche;
    }

    public String getMessage_info() {
        return message_info;
    }

    public int getId_joueur() {
        return id_joueur;
    }

    public int getNb_joueur() {
        return nb_joueur;
    }

    public ArrayList<Integer> getNb_carte_joueur() {
        return nb_carte_joueur;
    }

    public int getId_jeu() {
        return id_jeu;
    }

    public int getId_coup() {
        return id_coup;
    }

 */

    /**
     * Setters
     */
/*
    public void setAction(type_action action) {
        this.action = action;
    }

    public void setList_carte(int nb_carte, ArrayList<Card> list_carte) {
        this.nb_carte = nb_carte;
        this.liste_carte = new ArrayList<>(liste_carte);
    }

    public void setId_pioche(int id_pioche) {
        this.id_pioche = id_pioche;
    }

    public void setMessage_info(String message_info) {
        this.message_info = message_info;
    }

    public void setId_joueur(int id_joueur) {
        this.id_joueur = id_joueur;
    }

    public void setListeCarte(int nb_joueur, ArrayList<Integer> nb_carte_joueur) {
        this.nb_joueur = nb_joueur;
        this.nb_carte_joueur = new ArrayList<>(nb_carte_joueur);
    }

    public void setId_jeu(int id_jeu) {
        this.id_jeu = id_jeu;
    }

    public void setId_coup(int id_coup) {
        this.id_coup = id_coup;
    }

 */

    /**
     * Méthode to_Send
     * //Méthode de création du message à envoyer
     */
/*
    public String to_Send() {

        String message = id_jeu + ":" + id_coup + ":" + id_joueur + ":" + id_pioche + ":" + nb_carte + ":";

        // Ajout de la liste de cartes
        for (int i=0; i < nb_carte; i++) {
            message += liste_carte.get(i).idCard();
            if (i != nb_carte - 1) {
                message += ("/");
            }
        }

        // Ajout du nombre de joueur
        message += ":" + nb_joueur + ":";

        //Ajout du nombre de carte de chaque joueur
        for (int j=0; j < nb_joueur; j++) {
            message += Integer.toString(nb_carte_joueur.get(j));
            if (j != nb_joueur - 1) {
                message += "/";
            }
        }

        message += ":";

        if (message_info != "") {
            message += message_info;
        }

        int lenMessage = message.length();

        message = String.valueOf(action.ordinal()) + ":" + lenMessage + ":" + message;

        return message;
    }

 */

    /**
     * Méthode unpacked
     * Méthode de dépactage des informations
     */

   /* public void unpacked(String message) {

        String[] infoSplited;
        int state = 0;

        infoSplited = message.split(":", -1);

        while (state <= 10) {
            switch (state) {
                case 0:
                    state ++;
                    if (infoSplited[0] != "") {
                        action = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    int longueur;
                    if (infoSplited[1] != "") {
                        longueur = Integer.parseInt(infoSplited[1]);
                        if (longueur == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (infoSplited[2] != "") {
                        id_jeu = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (infoSplited[3] != "") {
                        id_coup = Integer.parseInt(infoSplited[3]);
                    }
                    break;
                case 4:
                    state ++;
                    if (infoSplited[4] != "") {
                        id_joueur = Integer.parseInt(infoSplited[4]);
                    }
                    break;
                case 5:
                    state ++;
                    if (infoSplited[5] != "") {
                        id_pioche = Integer.parseInt(infoSplited[5]);
                    }
                    break;
                case 6:
                    state ++;
                    if (infoSplited[6] != "") {
                        nb_carte = Integer.parseInt(infoSplited[6]);
                    }
                    break;
                case 7:
                    state ++;
                    String[] listCardsSplited = infoSplited[7].split("/");
                    if (nb_carte > 0) {
                        for (int j=0 ;j < nb_carte; j++) {
                            if (listCardsSplited[j] != "") {
                                Card card = new Card(listCardsSplited[j]);
                                liste_carte.add(card);
                            }
                        }
                    }
                    break;
                case 8:
                    state ++;
                    if (infoSplited[8] != "") {
                        nb_joueur = Integer.parseInt(infoSplited[8]);
                    }
                    break;
                case 9:
                    state ++;
                    String[] listCardsPlayersSplited = infoSplited[9].split("/");
                    if (nb_joueur > 0) {
                        for (int j=0; j < nb_joueur; j++) {
                            if (listCardsPlayersSplited[j] != "") {
                                nb_carte_joueur.add(Integer.parseInt(listCardsPlayersSplited[j]));
                            }
                        }
                    }

                    break;
                case 10:
                    state ++;
                    try {
                        if (infoSplited[10] != "") {
                            message_info = infoSplited[10];
                        }
                    }
                    catch (IllegalArgumentException e){
                        System.out.print("Pas d'infos transmises");
                    }

                    break;
                default:
                    System.out.print("Valeur de state incorrecte");
            }
        }
    }
}

    */


    public abstract class Paquet {

        public enum type_action {RIEN , ID, DISTRIB, ASK, ATTENTE, REMOVE, SKIP, PLAY, PIOCHE, INFO_IA, INFO_ECRAN, READY,
            PLAYER_NAME, CLIENT_PSEUDO, PLAYER_READY, PLAYER_TURN, END_GAME, COUP_ILLEGAL, START;

            @NonNull
            public String toString() {
                return Integer.toString(this.ordinal());
            }

        }

        protected int _msgLength;
        protected type_action _msgFunction;
        protected String _msg;

        public Paquet() {
            this._msgFunction = type_action.RIEN;
            this._msgLength = 0;
            this._msg = "";
        }

        public Paquet(type_action action) {
            this._msgFunction = action;
            this._msgLength = 0;
            this._msg = "";
        }

        public Paquet(Paquet paquet) {
            this._msgFunction = paquet._msgFunction;
            this._msgLength = paquet._msgLength;
            this._msg = paquet._msg;
        }


        public type_action get_msgFunction() {
            return _msgFunction;
        }


        abstract String encode();
        abstract void decode(String receivedMsg);


    }
