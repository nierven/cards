package com.example.gameversion3;

import java.util.Objects;

public class PseudoClientPaquet extends Paquet{

    private String _pseudo;
    private int _idClient;

    public PseudoClientPaquet(){
        super();
        _pseudo = "";
        _idClient = 0;
    }

    public PseudoClientPaquet(String pseudo, int idClient){
        super(type_action.CLIENT_PSEUDO);
        _idClient = idClient;
        _pseudo = pseudo;
    }

    public PseudoClientPaquet(PseudoClientPaquet paquet){
        super(paquet);
        _pseudo = paquet._pseudo;
        _idClient = paquet._idClient;
    }

    String get_pseudo(){
        return _pseudo;
    }

    int get_idClient(){
        return _idClient;
    }

    @Override
    String encode() {
        String message;
        message = _idClient + ":" + _pseudo;
        _msgLength = message.length();
        _msg = _msgFunction.toString() + ":" + _msgLength + ":" + message;
        return _msg;
    }

    @Override
    void decode(String receivedMsg) {
        String[] infoSplited;
        int state = 0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 3) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        _idClient = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (!Objects.equals(infoSplited[3], "")) {
                        _pseudo = infoSplited[3];
                    }
                    break;

            }
        }
    }
}
