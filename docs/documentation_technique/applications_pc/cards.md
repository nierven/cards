# Cards

Liste des enums :

- **COLOR** (Card.h) : Couleurs possibles pour une carte
- **VALUE** (Card.h) : Valeurs possibles pour une carte
- **ActionType** (Table.h) : Type d'action qu'un joueur peut exécuter

Liste des classes :

- **Card** (Card.h) : Carte à jouer avec une valeur et une couleur
- **ClassicDeck** (ClassicDeck.h) : Jeu de 52 cartes composé de cartes allant de 2 à 10, des as, valets, reines et rois
- **Pile** (Pile.h) : Pile de cartes, peut être utilisée pour décrire la main d'un joueur, une pioche ou une défausse par exemple
- **Player** (Player.h) : Description d'un joueur, sa main et quelques fonctions d'utilitée
- **Table** (Table.h) : Table de jeu, comporte la liste des joueurs, la liste des piles, et quelques attributs plus particuliers comme l'historique des coups joués et le classement atuel des joeurs

Une vieille version d'UML représentant cette librairie :

![cards_uml](../../architecture/classes.png)