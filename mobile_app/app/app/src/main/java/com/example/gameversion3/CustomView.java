package com.example.gameversion3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

/**
 * Sert uniquement à afficher les cartes sur la moitié basse de l'écran
 */

public class CustomView extends View {

    private LayerDrawable layerDrawable;
    private ArrayList<AnimatedDrawableCard> animatedDrawableCardList;

    public CustomView(Context context, AttributeSet attributeSet) {

        super(context, attributeSet);

        // Initialisation du layerDrawable contenant l'ensemble des images de cartes superposés
        Drawable[] layers = new Drawable[1];
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.seven_of_spades);
        layers[0] = drawable;
        animatedDrawableCardList = new ArrayList<>();
        layerDrawable = new LayerDrawable(layers);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        layerDrawable.draw(canvas);
    }

    public ArrayList<AnimatedDrawableCard> getAnimatedDrawableCardList() {
        return animatedDrawableCardList;
    }

    public void setAnimatedDrawableCardList(ArrayList<AnimatedDrawableCard> animatedDrawableCardList) {
        this.animatedDrawableCardList = animatedDrawableCardList;
    }

    public void update(LayerDrawable layerdrawable, ArrayList<AnimatedDrawableCard> listAnimatedDrawables) {
        layerDrawable = layerdrawable;
        animatedDrawableCardList = listAnimatedDrawables;
        invalidate();
    }

    public void setLayerDrawable(LayerDrawable layerdrawable) {
        layerDrawable = layerdrawable;
    }

    public LayerDrawable getLayerDrawable() {
        return layerDrawable;
    }

    @Override
    public boolean performClick() {
        super.performClick();
        return true;
    }
}

