/*
 * Pile.h
 *
 *  Created on: 5 oct. 2023
 *      Author: lorenzo
 */

#ifndef PILE_H_
#define PILE_H_
#include <random>
#include <list>
#include <vector>
#include "Card.h"

class Pile {
public:
	/*Constructors*/
	Pile();
	Pile(bool visib);
	Pile(const Pile &pile);
	Pile(const vector<Card>& c);
	/*Accessors*/
	int getNbCard() const;
	bool isVisible() const;
	bool isInsidePile(Card cardToFind) const;
	int findCard(Card &cardToFind);
	/*Methods*/
	void addCard(Card &c);
	void removeCard(int idCarte);
	void shuffle();
	void print();
	void sortPile();
	Card &operator[](int i);
	virtual ~Pile();
private:
	vector<Card> cards;
	bool visible; //Information if the player can have a look to his own hand



};

#endif /* MAIN_H_ */
