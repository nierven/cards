#pragma once

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <list>
#include <string>
#include<vector>

using namespace std;
class Visualization;

enum ActivityType { Setup, Game, Results, Error };

struct DrawableObject
{
	string name;
	sf::Drawable* drawable = NULL;
	int zIndex = 0;
};

class LayerTree
{
private:
	list<DrawableObject> _objects;

public:
	void Add(string name, sf::Drawable *drawable, int zIndex = 0)
	{
		_objects.push_back(DrawableObject{ name, drawable, zIndex });
		_objects.sort([](DrawableObject const& o1, DrawableObject const& o2) { return o1.zIndex < o2.zIndex; });
	}

	void Remove(string name)
	{
		_objects.remove_if([&](DrawableObject const& o) { return name == o.name; });
	}

	auto begin() { return _objects.begin(); }
	auto end() { return _objects.end(); }

	DrawableObject& operator[](string name) { return *find_if(_objects.begin(), _objects.end(), [&](DrawableObject const& object) { return name == object.name; }); }
	const DrawableObject& operator[](string name) const { return *find_if(_objects.begin(), _objects.end(), [&](DrawableObject const& object) { return name == object.name; }); }
};

class Activity
{
public:
	Activity(Visualization *visualization);
	virtual void Activate();
	virtual void Deactivate();

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void Render();

protected:
	sf::RenderWindow *_window;
	Visualization *_visualization;
	LayerTree _objects;

	vector<string>* _player_connectes_noms;
};