#pragma once
#include "Paquet.h"


class PlayerReadyPaquet : public Paquet
{
private:
	int _idPlayer;

public:

	PlayerReadyPaquet();
	PlayerReadyPaquet(int iDPlayer, int sizeMsg);
	PlayerReadyPaquet(const PlayerReadyPaquet& paquet);

	int getIDPlayer() const;

	string encode();
	void decode(string receivedMsg);
};

