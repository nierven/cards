#include "EndGamePaquet.h"

EndGamePaquet::EndGamePaquet() : Paquet()
{
	this->_playerName = {};
}

EndGamePaquet::EndGamePaquet(vector<string> playerName, int size) : Paquet(END_GAME, size)
{
	this->_playerName = playerName;
}

EndGamePaquet::EndGamePaquet(const EndGamePaquet& paquet) : Paquet(paquet)
{
	this->_playerName = paquet._playerName;
}

vector<string> EndGamePaquet::getRanking() const
{
	return this->_playerName;
}

string EndGamePaquet::encode()
{
	string message;

	message = to_string(this->_playerName.size()) + ":";

	for (int i = 0; i < this->_playerName.size(); i++) {
		message += _playerName[i];
		if (i != (this->_playerName.size() - 1)) {
			message += "/";
		}
	}

	this->_msgLength = message.length();

	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void EndGamePaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	int nbPayer = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 4)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					nbPayer = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				for (int j = 0; j < nbPayer; j++) {
					info = splitString(receivedMsg, &i);
					this->_playerName.push_back(info);
					if (j != nbPayer - 1)
						i++;
				}
				stateDecode++;
				break;
			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}
}


