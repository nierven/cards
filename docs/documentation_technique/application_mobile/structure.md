# Application Android

Disponible sous `mobile_app/app/app/`. Certaines librairies utilisées dans l'application sont des traductions directes vers du Java des librairies écrites en C++. Les informations importantes sur ces librairies sont disponibles ci-dessous :

- [Cards](../applications_pc/cards.md) : Modèle C++ pour des cartes à jouer
- [GameNetwork](../applications_pc/gamenetwork.md) : Modules de réseau