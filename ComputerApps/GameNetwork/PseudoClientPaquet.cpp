#include "PseudoClientPaquet.h"

PseudoClientPaquet::PseudoClientPaquet() : Paquet()
{
	this->_pseudo = "";
	this->_idClient = 0;
}

PseudoClientPaquet::PseudoClientPaquet(string pseudo, int idClient, int sizeMsg) : Paquet(CLIENT_PSEUDO, sizeMsg)
{
	this->_pseudo = pseudo;
	this->_idClient = idClient;
}

PseudoClientPaquet::PseudoClientPaquet(const PseudoClientPaquet& paquet) : Paquet(paquet)
{
	this->_pseudo = paquet._pseudo;
	this->_idClient = paquet._idClient;
}

string PseudoClientPaquet::getPseudo() const
{
	return this->_pseudo;
}

int PseudoClientPaquet::getIDClient() const
{
	return this->_idClient;
}

string PseudoClientPaquet::encode()
{
	string message;

	message = to_string(this->_idClient) + ":" + this->_pseudo;
	this->_msgLength = message.length();
	this->_msg = to_string(this->_msgFunction) + ":" + to_string(this->_msgLength) + ":" + message;

	return this->_msg;
}

void PseudoClientPaquet::decode(string receivedMsg)
{
	string info;
	int i = 0;
	int stateDecode = 0;
	int nbCard = 0;
	try
	{
		while ((receivedMsg[i] != '\0') && (stateDecode < 4)) {
			switch (stateDecode)
			{
			case(0):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgFunction = static_cast<type_action>(stoi(info));
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(1):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_msgLength = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(2):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_idClient = stoi(info);
					i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			case(3):
				info = splitString(receivedMsg, &i);
				if (info != "") {
					this->_pseudo = info;
					//i++;
					stateDecode++;
				}
				else {
					i++;
				}
				break;
			default:
				break;
			}
		}
	}
	catch (const std::exception&)
	{
		this->_msgFunction = RIEN;
	}

}
