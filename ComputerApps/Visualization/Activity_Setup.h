#pragma once

#include "Activity.h"
#include "Client.h"
#include "Paquet.h"
class Activity_Setup : public Activity
{
public:
	Activity_Setup(Visualization *visualization, Client* client);
	void Init();
	void Update();

	vector<string>* getListNames();

private:
	Client* _client;
	sf::Font _menuFont;
	void UpdateUI(PlayerNamePaquet* paquetNamePlayer);
};