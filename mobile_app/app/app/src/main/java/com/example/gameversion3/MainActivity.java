package com.example.gameversion3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mNameInput;
    private EditText mIPInput;
    private Button mPlayButton;
    static final String PSEUDO_CODE = "PSEUDO_CODE";
    static final String IP_CODE = "IP_CODE";
    private String pseudo = null;
    private SharedPreferences preferences;
    private static final String PREFERENCES_FILE = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mNameInput = findViewById(R.id.activity_main_name_input);
        mPlayButton = findViewById(R.id.activity_main_play_btn);

        mPlayButton.setEnabled(false);

        mIPInput = findViewById(R.id.activity_main_ip_input);



        preferences = getSharedPreferences(PREFERENCES_FILE, MODE_PRIVATE);

        pseudo = preferences.getString(PSEUDO_CODE, null);
        GameControllerPresident.IP_ADRESS = preferences.getString(IP_CODE, null);

        if (pseudo != null) {
            mNameInput.setText(pseudo);
            mIPInput.setText(GameControllerPresident.IP_ADRESS);
            mPlayButton.setEnabled(true);
        }

        mNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                pseudo = s.toString();

                mPlayButton.setEnabled(pseudo.length() > 0);
            }
        });

        mIPInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                GameControllerPresident.IP_ADRESS = s.toString();

                mPlayButton.setEnabled(GameControllerPresident.IP_ADRESS.length() > 0);
            }
        });

        mPlayButton.setOnClickListener(view -> {
            Intent startingGameActivity = new Intent(MainActivity.this, StartingGameActivity.class);
            startingGameActivity.putExtra(PSEUDO_CODE, pseudo);
            Log.i("Debug", "Démarrage de StartingGameActivity");
            startActivity(startingGameActivity);
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Sauvegarde le pseudo lorsque l'activité s'arrête
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PSEUDO_CODE, mNameInput.getText().toString());
        editor.putString(IP_CODE, mIPInput.getText().toString());
        editor.apply();

    }

}
