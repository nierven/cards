#include "Phase.h"
#include <thread>
#include <chrono>

using namespace std;

void Phase::play(Table& t)
{
	switch (_state)
	{
		case PhaseState::Begin:
		{
			begin(t);
			if (legal(t) == true)
				_state = PhaseState::Exec;
			break;
		}
		case PhaseState::Exec:
		{
			exec(t);//Phase_1J mini= new Phase_1J(table);
			_state = PhaseState::End;
			break;
		}
		case PhaseState::End:
		{
			end(t);//s'execute quand la phase du dessous a son exec qui en cr�e pas un autre
			_state = PhaseState::Begin;
			break;
		}
	}
}