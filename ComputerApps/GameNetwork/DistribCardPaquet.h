#pragma once
#include "Paquet.h"
#include "Card.h"
#include <vector>

class DistribCardPaquet : public Paquet
{
private:
	vector<Card> _listCard;

public:

	DistribCardPaquet();
	DistribCardPaquet(vector<Card> listcard, int sizeMsg);
	DistribCardPaquet(const DistribCardPaquet& paquet);

	vector<Card> getListCard() const;

	string encode();
	void decode(string receivedMsg);

};

