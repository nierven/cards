/*
 * Joueur.h
 *
 *  Created on: 7 oct. 2023
 *      Author: lorenzo
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include <string>

#include "Pile.h"
using namespace std;

class Player {
public:
	Player();
	Player(string name);
	Player(string name, bool visib);
	Player(Player const & j);
	/*Accesseur*/
	Pile *getHand();
	string getName();
	/*Méthode*/
	void playCard(int n, Pile &pile1, Pile &pile2);
	void playCard(Card card, Pile &pile1, Pile &pile2);
	void playCard(Card card, Pile* pile1, Pile* pile2);
	void playCards(vector<Card> cardsToPlay,Pile &pile1, Pile &pile2);
	void playCards(list<int> cardsToPlay,Pile &pile1, Pile &pile2);
	void print();
	virtual ~Player();
private:
	string name;
	Pile hand;
};

#endif /* PLAYER_H_ */
