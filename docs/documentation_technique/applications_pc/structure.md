# Applications PC

Disponible sous `ComputerApps/`. On y retrouve la structure suivante :

- Apps
    - [GameServer](gameserver.md) : Serveur de jeu (CLI)
    - [Visualization](visualization.md) : Fenêtre d'affichage (GUI)
- Librairies
    - [Cards](cards.md) : Modèle C++ pour des cartes à jouer
    - [GameNetwork](gamenetwork.md) : Modules de réseau
    - [GameRules](gamerules.md) : Règles de jeu (modèle abstrait & Président)

Les dépendances sont visibles sur l'image ci-dessous :

![structure](../../architecture/cards-Structure.png)