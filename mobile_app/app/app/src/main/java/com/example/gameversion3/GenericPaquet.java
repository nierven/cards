package com.example.gameversion3;

import java.util.Objects;

public class GenericPaquet extends Paquet{

    public GenericPaquet(){
        super();
    }

    public GenericPaquet(type_action action){
        super(action);
    }

    public GenericPaquet(GenericPaquet paquet){
        super(paquet);
    }

    @Override
    String encode() {
        String message = "";
        _msgLength = 0;
        _msg = _msgFunction.toString() + ":" + _msgLength + message;
        return _msg;
    }

    @Override
    void decode(String receivedMsg) {

        String[] infoSplited;
        int state = 0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 1) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
            }
        }
    }
}
