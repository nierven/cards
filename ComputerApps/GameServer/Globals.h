#pragma once

#include "Table.h"
#include "Serveur.h"
#include "Phase.h"

#include <map>

extern Table GameTable;
extern Serveur NetworkServer;
extern ServerStates CurrentState;