# GameServer

Le GameServer est géré par une machine à état principale (`GameServer.cpp`) qui permet d'alterner entre différentes phases de fonctionnement du serveur. À ne pas confondre avec des phases de jeu qui sont des phases élémentaires implémentées pour chaque jeu de cartes que l'on souhaite avoir. La machine à état principale est cadencée à 120 FPS.