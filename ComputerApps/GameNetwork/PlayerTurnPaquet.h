#pragma once
#include "Paquet.h"

class PlayerTurnPaquet : public Paquet
{	
private:
	int _idPlayer;
	string _pseudo;

public:
	PlayerTurnPaquet();
	PlayerTurnPaquet(int iDPaquet, string _pseudo, int sizeMsg);
	PlayerTurnPaquet(const PlayerTurnPaquet& paquet);

	int getIDPlayer() const;
	string getPseudo() const;

	string encode();
	void decode(string receivedMsg);
};

