/*
 * Pile.cpp
 *
 *  Created on: 5 oct. 2023
 *      Author: lorenzo
 */

#include "Pile.h"
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <stdexcept>
using namespace std;

Pile::Pile() {
	this->visible = false;
}

Pile::Pile(bool visib)
{
	this->visible = visib;
}

Pile::Pile(const Pile &main) {
	this->cards = main.cards;
	this->visible = main.isVisible();
}
Pile::Pile(const vector<Card>& c) {

	this->cards = c;
	this->visible = false;

}

int Pile::getNbCard() const {
	return cards.size();
}

bool Pile::isVisible() const {
	return this->visible;
}


bool Pile::isInsidePile(Card cardToTest) const {
	for (Card card : this->cards) {
		if (card == cardToTest) {
			return true;
		}
	}
	return false;
}


int Pile::findCard(Card &cardToFind){
	/**
	* Find a specific card inside the hand
	* 
	* @param cardToFind is the reference to the card to find
	* 
	* @return index of the card if the card is there and -1 if not
	*/
	for (int i=0; i<this->getNbCard(); i++){
		if((*this)[i] == cardToFind) {
			return i;
		}
	}
	return -1;
}

void Pile::addCard(Card &c) {
	cards.push_back(c);
}

void Pile::removeCard(int idCard) {
	if (idCard < this->getNbCard()) {
		this->cards.erase(this->cards.begin() + idCard);
	}
}

bool comp(Card &carte1, Card &carte2) {
	int v1 = carte1.getValue();
	int v2 = carte2.getValue();
	if (v1 < 3) {
		v1 += 13;
	}
	if (v2 < 3) {
		v2 += 13;
	}
	return v1 < v2;

}

void Pile::sortPile() {
	sort(this->cards.begin(), this->cards.end());
}

void Pile::print() {
	for (Card carte : this->cards) {
		carte.print();
	}
}

void Pile::shuffle() {
	srand((unsigned int) time(0));
	Pile randomPile;
	int cardToPickUp;
	int n = this->getNbCard();
	while (n != 0) {
		cardToPickUp = rand() % n;
		randomPile.addCard((*this)[cardToPickUp]);
		this->removeCard(cardToPickUp);
		n = this->getNbCard();
	}
	*this = randomPile;
}

Card &Pile::operator [](int i){
	if (i < this->getNbCard()) {
		if (i < 0) {
			return this->cards[this->getNbCard() + i];
		}
		return this->cards[i];
	}else {
		throw domain_error(
				"The argument exceeds the number of cards present in the hand");
	}
}

Pile::~Pile() {
	// TODO Auto-generated destructor stub
}

