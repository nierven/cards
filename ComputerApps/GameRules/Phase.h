#pragma once

#include "Card.h"
#include "Table.h"
#include <iostream>
#include "Client.h"
#include <vector>

enum PhaseState
{
	Begin,
	Exec,
	End
};

class Phase
{
public:
	static Phase* CurrentPhase;
	vector<Client*>* listClients;
	Phase() { listClients = {}; };
	void setListClient(vector<Client*>* listClientOld) {
		this->listClients = listClientOld;
	};
	void play(Table& t);

	virtual void begin(Table& t) = 0;
	virtual bool legal(Table& t) = 0;
	virtual void exec(Table& t) = 0;
	virtual void end(Table& t) = 0;

	virtual ~Phase() {};

private:
	PhaseState _state = PhaseState::Begin;
};