# Structure du projet

Le projet global est segmenté en plusieurs sous-projets :

- [ComputerApps](applications_pc/structure.md) : Applications & Librairies C++ pour Windows
- [mobile_app](application_mobile/structure.md) : Application mobile pour Android (Java et XML)

La documentation est gérée par [MkDocs](https://www.mkdocs.org) et les sources accessibles sous `docs`. Le dossier `SFML` regroupe les binaires, includes et librairies de la [SFML](https://www.sfml-dev.org), la librairie graphique que nous utilisons sous Windows.