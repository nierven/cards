#include "Card.h"
#include <string>
#include <iostream>

Card::Card() {
	this->color = Undefined;
	this->value = Zero;
}

Card::Card(COLOR c, VALUE v) {
	this->color = c;
	this->value = v;
}

Card::Card(int c, int v) {
	this->color = static_cast<COLOR>(c);
	this->value = static_cast<VALUE>(v);
}

Card::Card(const Card &c) {
	this->color = c.color;
	this->value = c.value;
}

Card::Card(string idCard)
{
	int i = 0;
	string c;
	string v;
	while (idCard[i] != ';') {
		c += idCard[i];
		i++;
	}
	i++;
	while (idCard[i] != '\0') {
		v += idCard[i];
		i++;
	}
	this->color = static_cast<COLOR>(stoi(c));
	this->value = static_cast<VALUE>(stoi(v));
}

string Card::toString() {
	string string_couleur;
	string string_valeur;
	switch (value) {
	case 0:
		string_valeur = "Zero";
		break;
	case 1:
		string_valeur = "Ace";
		break;
	case 2:
		string_valeur = "Two";
		break;
	case 3:
		string_valeur = "Three";
		break;
	case 4:
		string_valeur = "Four";
		break;
	case 5:
		string_valeur = "Five";
		break;
	case 6:
		string_valeur = "Six";
		break;
	case 7:
		string_valeur = "Seven";
		break;
	case 8:
		string_valeur = "Eight";
		break;
	case 9:
		string_valeur = "Nine";
		break;
	case 10:
		string_valeur = "Ten";
		break;
	case 11:
		string_valeur = "Jack";
		break;
	case 12:
		string_valeur = "Queen";
		break;
	case 13:
		string_valeur = "King";
		break;
	}
	switch (color) {
	case 0:
			string_couleur = "Undefined";
			break;
	case 1:
		string_couleur = "Hearts";
		break;
	case 2:
		string_couleur = "Spades";
		break;
	case 3:
		string_couleur = "Clubs";
		break;
	case 4:
		string_couleur = "Diamonds";
		break;
	}
	return string_valeur + " of " + string_couleur;
}

string Card::idCard()
{
	string s = to_string(this->color) + ';' + to_string(this->value);
	return s;
}

void Card::setColor(COLOR c) {
	this->color = c;
}

void Card::setColor(int c) {
	this->color = static_cast<COLOR>(c);
}

void Card::setValue(VALUE v) {
	this->value = v;
}

void Card::setValue(int v) {
	this->value = static_cast<VALUE>(v);
}


void Card::print() {
	std::cout << toString() << std::endl;
}

int Card::getColor() {
	return this->color;
}

int Card::getValue() {
	return this->value;
}

Card::~Card() {
	// TODO Auto-generated destructor stub
}

bool Card::operator ==(Card &main2) {
	bool test = true;
	if (this->color!=main2.getColor()){
		test = false;
	}
	if (this->value!=main2.getValue()){
		test = false;
	}
	return test;
}

bool Card::operator <(Card &card2) { // Need to override this function
	int value1 = this->value;
	int value2 = card2.value;
	if (value1 < 3) {
		value1 += 13;
	}
	if (value2 < 3) {
		value2 += 13;
	}
	if(value1 < value2){
		return true;
	}  else {
		return false;
	}
}


