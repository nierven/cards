#include "State_Setup.h"
#include <iostream>
#include <thread>
#include "Card.h"

static thread *threadConnection = NULL;
static thread* threadUdp = NULL;
int nbClientsConnected;

void State_Setup::onInit()
{	
	// Initialisation du serveur TCP
	cout << "State_Setup" << endl;
	threadConnection = new thread(&Serveur::listenConnection, &NetworkServer);
	threadUdp = new thread(&Serveur::broadcastUdp, &NetworkServer);
	nbClientsConnected = 0;
}

void State_Setup::Loop()
{
	// On attend la connexion des clients
	
	
	//Send Names to VISU
	if (NetworkServer.getnbClient() != nbClientsConnected)
	{
		vector<string> clientsName; 
		for (Client* c : *NetworkServer.getPtrListClist())
		{
			// Si nom est vide, alors sortir de la boucle et attendre
			// que le nom soit donn� par le client
			if (c->getName() == "")
			{
				return;
			}
			// Ajouter le nom du client � la liste
			else if (c->getName() != "Visu")
			{
				clientsName.push_back(c->getName());
			}
		}

		PlayerNamePaquet* names = new PlayerNamePaquet(clientsName, 0);

		//send to Visu
		if (findVisu(*NetworkServer.getPtrListClist()) != NULL) {
			Client* client = findVisu(*NetworkServer.getPtrListClist());
			client->addPaquetToSend(names);
		}

		nbClientsConnected = NetworkServer.getnbClient();
	}


	// Si tout le monde est pret, alors demarrer la partie
	if (NetworkServer.getnbReadyClient() >= 2 &&
		NetworkServer.getnbReadyClient() == NetworkServer.getnbClient() &&
		findVisu(*NetworkServer.getPtrListClist()) != NULL)
	{
		CurrentState = ServerStates::Game;
	}
}

void State_Setup::onExit()
{
	// Ajout des joueurs
	
	vector<Client*>* listPlayer = NetworkServer.getPtrListClist();
	for (Client* c : *listPlayer) {
		if (c->getName() != "Visu")
			GameTable.sitPlayer(c->getPlayer());
	}
}