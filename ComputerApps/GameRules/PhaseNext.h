#ifndef PHASENEXT_H_
#define PHASENEXT_H_

#include "Phase.h"
#include "Paquet.h"

class PhaseNext :
    public Phase
{
private:
    ACTION action;
    void updateUI(Table& t);


public:
    void begin(Table& t);
    bool legal(Table& t);
    void exec(Table& t);
    void end(Table& t);
};

#endif;