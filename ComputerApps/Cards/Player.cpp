/*
 * Player.cpp
 *
 *  Created on: 7 oct. 2023
 *      Author: lorenzo
 */

#include <string>
#include <iostream>

#include "Player.h"

Player::Player() {
	this->name = "";
	this->hand = Pile();
}

Player::Player(string name) {
	this->name = name;
	this->hand = Pile();
}

Player::Player(string name, bool visib)
{
	this->name = name;
	this->hand = Pile(visib);
}

Player::Player(Player const &j) {
	this->name = j.name;
	this->hand = j.hand;
}

Pile* Player::getHand() {
	return &(this->hand);
}

string Player::getName() {
	return this->name;
}

void Player::playCard(int n, Pile &pile1, Pile &pile2) {
	pile2.addCard(pile1[n]);
	pile1.removeCard(n);
}

void Player::playCard(Card card, Pile &pile1, Pile &pile2) {
	int n=pile1.findCard(card);
	pile2.addCard(pile1[n]);
	pile1.removeCard(n);
}

void Player::playCard(Card card, Pile* pile1, Pile* pile2) {
	int n = pile1->findCard(card);
	pile2->addCard( (*pile1)[n] );
	pile1->removeCard(n);
}

void Player::playCards(vector<Card> cardsToPlay, Pile &pile1, Pile &pile2){
	for (Card card : cardsToPlay){
		this->playCard(card,pile1,pile2);
	}
}

void Player::playCards(list<int> cardsToPlay, Pile &pile1, Pile &pile2){
	for (int card : cardsToPlay){
		this->playCard(card,pile1,pile2);
	}
}

void Player::print() {
	std::cout << "------------------" << std::endl;
	std::cout << name + "'s hand : " << std::endl;
	if (this->hand.isVisible()) {
		this->hand.print();
	} else {
		for (int i = 0; i < this->hand.getNbCard(); i++) {
			std::cout << "*** of ***" << std::endl;
		}
	}
	std::cout << "------------------" << std::endl;

}

Player::~Player() {
	// TODO Auto-generated destructor stub
}

