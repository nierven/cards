package com.example.gameversion3;

import java.util.ArrayList;
import java.util.Objects;

public class DistribCardPaquet extends Paquet{

    private final ArrayList<Card> _listCard;

    public DistribCardPaquet(){
        super();
        _listCard = new ArrayList<>();
    }

    public DistribCardPaquet(ArrayList<Card> listCard){
        super();
        _listCard = listCard;
    }

    public DistribCardPaquet(DistribCardPaquet paquet){
        super(paquet);
        _listCard = paquet._listCard;
    }

    public ArrayList<Card> get_listCard(){
        return _listCard;
    }

    @Override
    String encode() {
        String message;
        message = _listCard.size() + ":";

        for(int i = 0; i<_listCard.size(); i++){
            message += _listCard.get(i).idCard();
            if( i != _listCard.size() - 1){
                message += "/";
            }
        }
        _msgLength = message.length();
        _msg = _msgFunction.toString() + ":" + _msgLength + ":" + message;
        return _msg;
    }

    @Override
    void decode(String receivedMsg) {

        String[] infoSplited;
        int state = 0;
        int nbCard=0;

        infoSplited = receivedMsg.split(":", -1);

        while (state <= 3) {
            switch (state) {
                case 0:
                    state ++;
                    if (!Objects.equals(infoSplited[0], "")) {
                        _msgFunction = type_action.values()[Integer.parseInt(infoSplited[0])];
                    }
                    break;
                case 1:
                    state ++;
                    if (!Objects.equals(infoSplited[1], "")) {
                        _msgLength = Integer.parseInt(infoSplited[1]);
                        if (_msgLength == 0) {
                            System.out.print("Longueur de message nulle");
                        }
                    }
                    break;
                case 2:
                    state ++;
                    if (!Objects.equals(infoSplited[2], "")) {
                        nbCard = Integer.parseInt(infoSplited[2]);
                    }
                    break;
                case 3:
                    state ++;
                    if (!Objects.equals(infoSplited[3], "")) {
                        String[] card = infoSplited[3].split("/");
                        for(int j = 0; j<nbCard; j++){
                            _listCard.add(new Card(card[j]));
                        }
                    }
                    break;
                default:
                    System.out.print("Erreur de décodage");
            }
        }
    }
}
