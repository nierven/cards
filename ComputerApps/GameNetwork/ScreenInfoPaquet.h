#pragma once
#include "Paquet.h"
#include <vector>
#include "Card.h"

class ScreenInfoPaquet : public Paquet
{
private: 
	vector<Card> _listCard;
	vector<int> _nbCardPlayer;

public: 

	ScreenInfoPaquet();
	ScreenInfoPaquet(vector<Card> listCard, vector<int> nbCardPlayer, int sizeMsg);
	ScreenInfoPaquet(const ScreenInfoPaquet& paquet);

	vector<Card> getListCard() const;
	vector<int>  getNbCardPlayer() const;

	string encode();
	void decode(string receivedMsg);
	
};

